# Parcel Management Server Documentation

# Server API

## Contents

* Overseer's Part
    * [/overseer/sign/in](#signinoverseer)
    * [/overseer/log/out](#logoutoverseer)
    * [/overseer/manager/new/register](#registermanager)
    * [/overseer/manager/all/get](#getallmanagers)
    
* Manager's Part
    * [/manager/sign/in](#signinmanager)
    * [/manager/log/out](#logoutmanager)
    * [/manager/operator/new/register](#registeroperator)
    * [/manager/driver/new/register](#registerdriver)
    * [/manager/operator/assign/parcel](#assignoperator)
    * [/manager/driver/assign/parcel](#assigndriver)
    * [/manager/operator/all/get](#getalloperators)
    * [/manager/driver/all/get](#getalldrivers)
    * [/manager/parcel/all/get](#getallcurrentparcels)
    * [/manager/parcel/all/archived/get](#getallarchivedparcels)
    * [/manager/assessment/get/criteria](#getassessmentcriteria)
    * [/manager/assessment/assess/parcel](#assessparcel)
    * [/manager/statistics/get](#getstatistics)
    
* Customer's part
    * [/customer/register](#registercustomer)
    * [/customer/sign/in](#signincustomer)
    * [/customer/log/out](#logoutcustomer)
    * [/customer/create/delivery](#createnewdelivery)
    * [/customer/get/deliveries](#getcurrentdeliveries)
    * [/customer/get/archived/deliveries](#getarchiveddeliveries)
    * [/customer/confirmation_number/get](#getconfirmationnumber)
    * [/customer/assess/delivery](#assessparcelcustomer)

* Operator's part
    * [/operator/sign/in](#signinoperator)
    * [/operator/log/out](#logoutoperator)
    * [/operator/delivery/get](#getthedelivery)
    * [/operator/get/deliveries](#getassignedcurrentdeliveries)
    * [/operator/get/archived/deliveries](#getassignedarchiveddeliveries)
    * [/operator/get/new/deliveries](#getnewdeliveries)
    * [/operator/confirm/delivery](#confirmdelivery)
    * [/operator/reject/delivery](#rejectdelivery)
    * [/operator/edit/delivery](#editdelivery)
    * [/operator/drivers/parcels/get](#getdriversandparcels)
    
* Driver's part
    * [/driver/sign/in](#signindriver)
    * [/driver/log/out](#logoutdriver)
    * [/driver/get/deliveries](#getdriverdeliveries)
    * [/driver/confirm/delivery](#driverconfirmdelivery)
    * [/driver/complete/delivery](#drivercompletedelivery)
    * [/driver/pick_up/delivery](#driverpickupdelivery)
    * [/driver/emergency/button](#sendemergency)

* Utilities api
    * [/](#isapiup)
    * [/db/drop](#dropdb)
    
## Overseer's Part

***

### signInOverseer

Sign in Overseer into the system

Method | Address
-------|--------
GET    |/overseer/sign/in

Parameter | Type | Description
----------|------|------------
email     |string|Overseer's email
password  |string|Overseer's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### logOutOverseer

End current Overseer's session


Method | Address
-------|--------
PUT    |/overseer/log/out

Parameter | Type | Description
----------|------|------------
token     |string|Overseer's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### registerManager

Register a new Manager in the system

Method | Address
-------|--------
PUT    |/overseer/manager/new/register

Parameter | Type | Description
----------|------|------------
name      |string|Manager's name
email     |string|Manager's email
password  |string|Manager's password
token     |string|Overseer's token


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3
  }
}
```

***

### getAllManagers

Get each managers' information, logged in and not

Method | Address
-------|--------
GET    |/overseer/manager/all/get

Parameter | Type | Description
----------|------|------------
token     |string|Overseer's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id" : 2,
      "name" : "Kolya",
      "email" : "foo@bar.com",
      "is_online" : true
    },
    ...
  ]
}
```

***

## Manager's Part
This API can also be used by an Overseer: just pass his token instead of Manager's one

***

### signInManager

Sign in Manager to the system

Method | Address
-------|--------
GET    |/manager/sign/in

Parameter | Type | Description
----------|------|------------
email     |string|Manager's email
password  |string|Manager's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### logOutManager

End current Manager's session


Method | Address
-------|--------
PUT    |/manager/log/out

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### registerOperator

Register a new Operator in the system

Method | Address
-------|--------
PUT    |/manager/operator/new/register

Parameter | Type | Description
----------|------|------------
name      |string|Operator's name
email     |string|Operator's email
password  |string|Operator's password
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3
  }
}
```

***

### registerDriver

Register a new Driver in the system

Method | Address
-------|--------
PUT    |/manager/driver/new/register

Parameter | Type | Description
----------|------|------------
name      |string|Driver's name
email     |string|Driver's email
password  |string|Driver's password
phone     |string|Driver's phone
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3
  }
}
```

***

### assignOperator

Assign an Operator to a parcel. Removes the previous operator, if he was assigned earlier

Method | Address
-------|--------
PUT    |/manager/operator/assign/parcel

Parameter | Type | Description
----------|------|------------
parcel_id |string|Id of the parcel
op_id     |string|Id of the operator
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### assignDriver

Assign a Driver to a parcel. Removes the previous driver, if he was assigned earlier

Method | Address
-------|--------
PUT    |/manager/driver/assign/parcel

Parameter | Type | Description
----------|------|------------
parcel_id |string|Id of the parcel
dr_id     |string|Id of the driver
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getAllOperators

Get each operators' information, logged in and not, with IDs of parcels, which are assigned to them

Method | Address
-------|--------
GET    |/manager/operator/all/get

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id" : 2,
      "name" : "Kolya",
      "is_online" : true,
      "parcels" :
      [
        4, 6, 7, ...
      ]
    },
    ...
  ]
}
```

***

### getAllDrivers

Get each driver' information, logged in and not, with IDs of parcels, which are assigned to them

Method | Address
-------|--------
GET    |/manager/driver/all/get

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id" : 2,
      "name" : "Kolya",
      "phone" : "+79991234567",
      "is_online" : true,
      "parcels" :
      [
        4, 6, 7, ...
      ]
    },
    ...
  ]
}
```

***

### getAllCurrentParcels

Get all parcels, which are currently processed in the system

Method | Address
-------|--------
GET    |/manager/parcel/all/get

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "destination": "Foo str, 2-3",
      "urgency": "low",
      "type": "special",
      "status": "created",
      "ass_operator" : 10,
      "ass_driver" : 15
    },
    ...
  ]
}
```

***

### getAllArchivedParcels

Get all parcels, which are archived in the system

Method | Address
-------|--------
GET    |/manager/parcel/all/archived/get

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "destination": "Foo str, 2-3",
      "urgency": "low",
      "type": "special",
      "status": "archived",
      "ass_operator" : 10,
      "ass_driver" : 15
    },
    ...
  ]
}
```

***

### getAssessmentCriteria

Get various assessment criteria for a specific parcel

Method | Address
-------|--------
GET    |/manager/assessment/get/criteria

Parameter | Type | Description
----------|------|------------
parcel_id |int   |Parcel's id
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  {
    "time_created" = "\/Date(1509133169270)\/",
    "time_confirmed_by_operator" = "\/Date(1509133169270)\/",
    "time_confirmed_by_driver" = "\/Date(1509133169270)\/",
    "time_picked_up" = "\/Date(1509133169270)\/",
    "time_delivered" = "\/Date(1509133169270)\/",
    "time_cust_assessed" = "\/Date(1509133169270)\/",
    "time_assessed" = "\/Date(1509133169270)\/",
    "time_rejected" = "\/Date(1509133169270)\/",
    "cust_mark" = 3.5,
    "cust_comment" = "Long delivery",
    "reject_msg_cust" = "Provided address does not exist",
    "reject_msg_emp" = "This customer wrote some garbage in address line"
  }
}
```

***

### assessParcel

Assess the parcel

Method | Address
-------|--------
PUT    |/manager/assessment/assess/parcel

Parameter | Type | Description
----------|------|------------
parcel_id |int   |Parcel's id
mark      |float |Mark in 0-5 range given by the Manager
comment   |string|Comment from the Manager
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getStatistics

Get the overall statistics about the system

Method | Address
-------|--------
GET    |/manager/statistics/get

Parameter | Type | Description
----------|------|------------
token     |string|Manager's token


**Response**

```
{
  "status" : "ok",
  "result" :
  {  
    "users" :
    {
      "op_total" = 5,
      "op_online" = 3,
      "dr_total" = 10,
      "dr_online" = 9,
      "managers_total" = 2,
      "managers_online" = 1
    },
    "parcels" : 
    {
      "parcels_total" = 4096,
      "parcels_created" = 128,
      "parcels_op_confirmed" = 256,
      "parcels_dr_confirmed" = 512,
      "parcels_picked_up" = 1024,
      "parcels_delivered" = 2048,
      "parcels_cust_assessed" = 439,
      "parcels_archived" = 64,
      "parcels_rejected" = 64,
      "op_avg_parcels" = 820,
      "dr_avg_parcels" = 409
    },
    "intervals" :
    {
      "avg_delivery_time" = 84184  // in seconds
    }
  }
}
```

***

## Customer's Part

### registerCustomer

Register a new Customer in the system

Method | Address
-------|--------
PUT    |/customer/register

Parameter | Type | Description
----------|------|------------
name      |string|Customer's name
phone     |string|Customer's phone
email     |string|Customer's email
password  |string|Customer's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### signInCustomer

Sign in Customer to the system

Method | Address
-------|--------
GET    |/customer/sign/in

Parameter | Type | Description
----------|------|------------
email     |string|Customer's email
password  |string|Customer's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### logOutCustomer

End current Customer's session


Method | Address
-------|--------
PUT    |/customer/log/out

Parameter | Type | Description
----------|------|------------
token     |string|Customer's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### createNewDelivery

Create a new delivery order, which will be inserted into a database

Method | Address
-------|--------
PUT    |/customer/create/delivery

Parameter      | Type | Description
---------------|------|------------
token          |string|Customer's token
del_name       |string|Some name given by the Customer for this delivery; can be a short description
urgency        |string|Urgency of the parcel (low, medium, high)
type           |string|Type of the parcel (usual, special)
src_address    |string|String address of the source
src_address_2  |string|Extra line of the address (apartment, office)
src_latitude   |string|Latitude of the source
src_longitude  |string|Longitude of the source
dest_address   |string|String address of the destination
dest_address_2 |string|Extra line of the address
dest_latitude  |string|Latitude of the destination
dest_longitude |string|Longitude of the destination

**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getCurrentDeliveries

Get list of all current deliveries for a particular Customer

Method | Address
-------|--------
GET    |/customer/get/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Customer's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "destination": "Foo str, 2-3",
      "urgency": "low",
      "type": "special",
      "status": "created",
      "reject_msg": "Address was faulty, and Customer did not answer the phone",
      "conf_num": 1814
    },
    ...
  ]
}
```

***

### getArchivedDeliveries

Get list of all archibed deliveries for a particular Customer

Method | Address
-------|--------
GET    |/customer/get/archived/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Customer's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "destination": "Foo str, 2-3",
      "urgency": "low",
      "type": "special",
      "status": "archived",
      "reject_msg": "Address was faulty, and Customer did not answer the phone",
      "conf_num": 1814
    },
    ...
  ]
}
```

***

### getConfirmationNumber

Get 4-digit confirmation number, which is to be entered by the Customer, when he gets a parcel

Method | Address
-------|--------
GET    |/customer/confirmation_number/get

Parameter | Type | Description
----------|------|------------
token     |string|Customer's token
parcel_id |int   |Parcel id


**Response**

```
{
  "status" : "ok",
  "result" : 3183
}
```

***

### assessParcelCustomer

Let the Customer assess the parcel before the Manager does so

Method | Address
-------|--------
PUT    |/customer/assess/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Customer's token
parcel_id |int   |Parcel id
mark      |float |Customer's mark
comment   |string|Customer's comment


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

## Operator's Part

### signInOperator

Sign in Operator to the system

Method | Address
-------|--------
GET    |/operator/sign/in

Parameter | Type | Description
----------|------|------------
email     |string|Operator's email
password  |string|Operator's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### logOutOperator

End current Operator's session


Method | Address
-------|--------
PUT    |/operator/log/out

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getTheDelivery

Get a specific delivery by its ID

Method | Address
-------|--------
GET    |/operator/delivery/get

Parameter | Type | Description
----------|------|------------
p_id      |int   |Id of the delivery
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" :
  {
    "id": 6,
    "name": "Toys",
    "source": "Vaskova str, 1-2",
    "src_lat": "24.56",
    "src_lon": "34.32",
    "destination": "Foo str, 2-3",
    "dest_lat": "12.56",
    "dest_lon": "43.90",
    "urgency": "low",
    "type": "special",
    "status": "Created",
    "driver": 2,
    "reject_msg_cust" = "Provided address does not exist",
    "reject_msg_emp" = "This customer wrote some garbage in address line"
  }
}
```

***

### getAssignedCurrentDeliveries

Get list of all current deliveries, which were assigned to the Operator

Method | Address
-------|--------
GET    |/operator/get/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "src_lat": "24.56",
      "src_lon": "34.32",
      "destination": "Foo str, 2-3",
      "dest_lat": "12.56",
      "dest_lon": "43.90",
      "urgency": "low",
      "type": "special",
      "status": "Created",
      "driver": 2,
      "reject_msg_cust" = "Provided address does not exist",
      "reject_msg_emp" = "This customer wrote some garbage in address line"
    },
    ...
  ]
}
```

***

### getAssignedArchivedDeliveries

Get list of all archived deliveries, which were assigned to the Operator

Method | Address
-------|--------
GET    |/operator/get/archived/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "src_lat": "24.56",
      "src_lon": "34.32",
      "destination": "Foo str, 2-3",
      "dest_lat": "12.56",
      "dest_lon": "43.90",
      "urgency": "low",
      "type": "special",
      "status": "Archived",
      "driver": 2,
      "reject_msg_cust" = "Provided address does not exist",
      "reject_msg_emp" = "This customer wrote some garbage in address line"
    },
    ...
  ]
}
```

***

### getNewDeliveries

Get list of all deliveries, assigned to this Operator, which were not confirmed yet

Method | Address
-------|--------
GET    |/operator/get/new/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "operator_id": 23,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "src_lat": "24.56",
      "src_lon": "34.32",
      "destination": "Foo str, 2-3",
      "dest_lat": "12.56",
      "dest_lon": "43.90",
      "urgency": "low",
      "type": "special",
      "status": "Created"
    },
    ...
  ]
}
```

***

### confirmDelivery

Confirm and sign a delivery by Operator

Method | Address
-------|--------
PUT    |/operator/confirm/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token
parcel_id |int   |Delivery's id
driver_id |int   |Id of the chosen driver
route_id  |string|Id of the chosen route

Here, in response, the External service error can happen. That would mean that parcel was confirmed, driver was
assigned, but route was not confirmed

**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### rejectDelivery

Reject the incoming delivery

Method | Address
-------|--------
PUT    |/operator/reject/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token
id        |int   |Delivery's id
op_reject_msg  |string|Reject message for operator
cust_reject_msg|string|Reject message for customer


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### editDelivery

Edit the delivery, which is assigned to this Operator. All of the fields, even the unchanged ones, are to be completed

Method | Address
-------|--------
PUT    |/operator/edit/delivery

Parameter      | Type | Description
---------------|------|------------
token          |string|Operator's token
id             |int   |Delivery's id
del_name       |string|Delivery's name
urgency        |string|Delivery's urgency
type           |string|Delivery's type
src_address    |string|String address of the source
src_address_2  |string|Extra line of the address (apartment, office)
src_latitude   |string|Latitude of the source
src_longitude  |string|Longitude of the source
dest_address   |string|String address of the destination
dest_address_2 |string|Extra line of the address (apartment, office)
dest_latitude  |string|Latitude of the destination
dest_longitude |string|Longitude of the destination

**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getParcelRoutes

Get a couple of routes for a specified parcel

Method | Address
-------|--------
GET    |/operator/parcel/routes/get

Parameter | Type | Description
----------|------|------------
parcel_id |int   |Id of the parcel
token     |string|Operator's token


**Response**

```
{
  "status" : "ok",
  "result" : 
  [
    {
      "id": "48ffjdkl49f",
      "route":
      {
        route object from Google
      }
    },
    ...
  ]
}
```

***

### getDriversAndParcels

Get a parcel-to-driver distribution for a specific operator

Method | Address
-------|--------
GET    |/operator/drivers/parcels/get

Parameter | Type | Description
----------|------|------------
token     |string|Operator's token

In the result, there is a Dictionary<DriverId, List<ParcelId>> with only drivers and parcels, which are assigned to this Operator. Pay attention that drivers' ids are strings

**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "3": [ 4, 7, 9 ],
    "5": [ 1, 2, 3 ],
    ...
  }
}
```

***

## Driver Part

### signInDriver

Sign in Driver to the system

Method | Address
-------|--------
GET    |/driver/sign/in

Parameter | Type | Description
----------|------|------------
email     |string|Driver's email
password  |string|Driver's password


**Response**

```
{
  "status" : "ok",
  "result" : 
  {
    "id" : 3,
    "token" : "FHEfdjfe3fj3HF"
  }
}
```

***

### logOutDriver

End current Driver's session


Method | Address
-------|--------
PUT    |/driver/log/out

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### getDriverDeliveries

Get list of all deliveries, which were assigned to the Driver

Method | Address
-------|--------
GET    |/driver/get/deliveries

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token


**Response**

```
{
  "status" : "ok",
  "result" :
  [
    {
      "id": 6,
      "name": "Toys",
      "source": "Vaskova str, 1-2",
      "destination": "Foo str, 2-3",
      "urgency": "low",
      "type": "special",
      "status": "created"
    },
    ...
  ]
}
```

***

### driverConfirmDelivery

Confirm delivery by Driver

Method | Address
-------|--------
PUT    |/driver/confirm/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token
id        |int   |Delivery's id


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### driverPickUpDelivery

Pick up the delivery

Method | Address
-------|--------
PUT    |/driver/pick_up/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token
id        |int   |Delivery's id


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### driverCompleteDelivery

Complete delivery by Customer from the Driver's devide

Method | Address
-------|--------
PUT    |/driver/complete/delivery

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token
id        |int   |Delivery's id
conf_num  |int   |Delivery's confirmation number


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

### sendEmergency

Send an Emergency signal to the Operator

Method | Address
-------|--------
GET    |/driver/emergency/button

Parameter | Type | Description
----------|------|------------
token     |string|Driver's token


**Response**

```
{
  "status" : "ok",
  "result" : null
}
```

***

## Map Service API

### getDriverByToken

Get the driver id by his token

Method | Address
-------|--------
GET    |/maps/driver/id/get

Parameter     | Type | Description
--------------|------|------------
driver_token  |string|Driver's token
service_token |string|Secure service token


**Response**

```
{
  "status" : "ok",
  "result" : 42
}
```

***

## Utilities API

### isAPIUp

Check, if API is up

Method | Address
-------|--------
GET    |/

Parameter | Type | Description
----------|------|------------
          |      |


**Normal Response**

```
Welcome to Server API!
```

***

### dropDB

Drop the server DB. Please, be attentive, when using this function

Method | Address
-------|--------
GET    |/db/drop

Parameter | Type | Description
----------|------|------------
          |      |


**Normal Response**

```
DB is dropped
```

***

## Pre-created users

Role       |Email-password-?token
-----------|--------------
Supervisor |super@dooper.com - super
Manager    |manager@mail.com - manager
Operator   |op@mail.com - operator - w392fjed3jd932
Driver     |dr@mail.com - driver

## Possible status strings

Status string value |Meaning
--------------------|--------
ok                  |Operation was successfully completed
no_email            |No customer with such email is registered in the system
wrong_pswd          |Entered password is wrong
wrong_tok           |Token for this operation is incorrect
int_error           |Some internal error happened on the server
ext_service_error   |Some error happened on another system service
wrong_op            |Operator, who was not assigned to this parcel, trying to make changes in it
wrong_cust          |Customer, who has not created this parcel, trying to make changes in it
wrong_driver        |Driver, who was not assigned to this parcel, trying to make changes in it
wrong_conf_num      |Confirmation number entered is not valid
registered_alr      |User with such email or other unique parameter is already registered
no_op               |No operator is found
no_dr               |No driver is found
no_parcel           |No parcel with such parameters can be found
parcel_not_delivered_or_rejected|Parcel cannot be assessed, as it was not completed
wrong_status_change |Attempt to do a wrong status change (incorrect order)
no_op_online        |There is no operator online now

***

## Possible parcels' statuses

Parcel status value |Meaning
--------------------|--------
Created             |Parcel was created, and noone confirmed it yet
ConfirmedByOperator |Parcel was confirmed by an Operator
ConfirmedByDriver   |Parcel was confirmed by a Driver
PickedUp            |Parcel was picked up and is being delivered by a Driver
Delivered           |Parcel was delivered, and Customer confirmed it
AssessedByCustomer  |Parcel was assessed by the Customer
Archived            |Parcel was assesed and archived
Rejected            |Parcel was rejected by the Operator

***

# Build & Run Instructions:
1. Make sure you have:
a. .NET framework 4.5 or greater if running on Windows, or Mono package 5.2 or greater; 
b. PostgreSQL server setup on your machine with the following user-password: "pmanager", "1234"; 
c. Npgsql driver 3.0.5 or greater. Often it's going with PSQL package
2. To build a project, you need just to type in a terminal: 'msbuild \<path-to-*.sln\>'; the msbuild utilite is going with a Mono
3. To run a project, type: 'mono \<path-to-*.exe\>'. The .exe file will be located inside a bin folder

# THE END
