﻿using System;
using System.Linq;
using Nancy;
using NUnit.Framework;
using ParcelsManagementServer.Database;
using Nancy.Testing;
using ParcelsManagementServer.Server;
using ParcelsManagementServer.Server.Controllers;
using System.Web.Helpers;
using ParcelsManagementServer.Server.Utilities;

namespace Tests
{
    [TestFixture]
    public class APITests
    {
        /// <inheritdoc />
        /// <summary>
        /// Creates a custom boostrapper for Nancy to init the API in tests from the
        /// same sources it is inited in main program
        /// </summary>
        private class MyBootstrapper : ConfigurableBootstrapper
        {
            /// <summary>
            /// Modules to be initied via this boostrapper
            /// </summary>
            private static readonly Action<ConfigurableBootstrapperConfigurator> nancyModules = 
                with => {
                    with.Module<UtilitiesController>();
                    with.Module<CustomerController>();
                    with.Module<OperatorController>();
                    with.Module<DriverController>();
                };

            public MyBootstrapper() : base(nancyModules)
            {
            }
        }
        
        [Test]
        public void ApiIsUp()
        {
            var browser = new Browser(new MyBootstrapper());
            var result = browser.Get("/", with =>
            {
                with.HttpRequest();
            });
            
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }
        
        [TestFixture]
        public class CustomerAPITests
        {     
            private readonly Browser browser = new Browser(new MyBootstrapper());

            [SetUp]
            public void Init()
            {
                // Drop DB before any test
                var db = new ParcelsManagementDBContext();
                db.Database.Delete();
            }

            [Test]
            public void CanRegister()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register two customers and check their existance (exception will
                // be thrown, if they do not exist)
                var registerResponse1 = browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust1 = db.Customers.First(c => c.Email == "foo1@bar.ru");
                var deserealizedLogInResponse1 = Json.Decode(registerResponse1.Result.Body.AsString());
                
                var registerResponse2 = browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "bar123bar");
                    with.HttpRequest();
                });
                var cust2 = db.Customers.First(c => c.Email == "foo2@bar.ru");
                var deserealizedLogInResponse2 = Json.Decode(registerResponse2.Result.Body.AsString());
                
                // Compare their passwords' hashes to what we have entered
                Assert.AreEqual(cust1.Password, ServerUtilities.GetHash("foo123foo"));
                Assert.AreEqual(cust2.Password, ServerUtilities.GetHash("bar123bar"));
                
                // Compare returned tokens with ones in DB
                Assert.AreEqual(cust1.Token, deserealizedLogInResponse1["result"]["token"]);
                Assert.AreEqual(cust2.Token, deserealizedLogInResponse2["result"]["token"]);
            }

            [Test]
            public void CanLogIn()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register customer
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in as this customer
                var logInResponse = browser.Get("/customer/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var deserealizedLogInResponse = Json.Decode(logInResponse.Result.Body.AsString());
                Assert.AreEqual("ok", deserealizedLogInResponse["status"]);
                
                // Check returned token
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                var result = deserealizedLogInResponse["result"];
                Assert.AreEqual(result["token"], cust.Token);
            }

            [Test]
            public void CanLogOut()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register customer
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in and check token exists
                browser.Get("customer/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                Assert.NotNull(cust.Token);

                // End customer session and check token was nullified
                browser.Put("/customer/log/out", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.HttpRequest();
                });
                db.Entry(cust).Reload();
                Assert.IsNull(cust.Token);
            }
            
            [Test]
            public void CanCreateParcel()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register customer and make him create a parcel
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust1 = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust1.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                
                // Check parcel was created
                Assert.True(cust1.Parcels.Any(p => p.Name == "Toys" && p.SourceAddress == "Kolotushkina str"));
                
                // Append another parcel to this customer and check it was really appended
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust1.Token);
                    with.FormValue("del_name", "Parts");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                db.Entry(cust1).Collection(c => c.Parcels).Load(); // Reload list to have an updated one
                Assert.True(cust1.Parcels.Count >= 2 &&
                            cust1.Parcels.Any(p => p.Name == "Parts" && p.SourceAddress == "Kolotushkina str"));

                // Register a new customer, create his parcel and check it was created with this customer 
                // and another doesn't know about it
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust2 = db.Customers.First(c => c.Email == "foo2@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust2.Token);
                    with.FormValue("del_name", "Colours");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                db.Entry(cust1).Collection(c => c.Parcels).Load();
                Assert.True(cust2.Parcels.Any(p => p.Name == "Colours" && p.SourceAddress == "Kolotushkina str"));
                Assert.False(cust1.Parcels.Any(p => p.Name == "Colours" && p.SourceAddress == "Kolotushkina str"));
            }

            [Test]
            public void CanGetCreatedParcels()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register customer and add his parcel
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                var parcelResponse = browser.Get("/customer/get/deliveries", with =>
                {
                    with.Query("token", cust.Token);
                    with.HttpRequest();
                });
                
                // Get his parcel and check its name
                var desParcelResponse = Json.Decode(parcelResponse.Result.Body.AsString());
                Assert.AreEqual(desParcelResponse["status"], "ok");
                Assert.AreEqual(desParcelResponse["result"][0]["name"], "Toys");
                
                // Add another parcel and check its name
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Barrels");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                parcelResponse = browser.Get("/customer/get/deliveries", with =>
                {
                    with.Query("token", cust.Token);
                    with.HttpRequest();
                });
                
                desParcelResponse = Json.Decode(parcelResponse.Result.Body.AsString());
                Assert.AreEqual(desParcelResponse["status"], "ok");
                Assert.AreEqual(desParcelResponse["result"][1]["name"], "Barrels");
            }
        }

        [TestFixture]
        public class OperatorAPITests
        {      
            private readonly Browser browser = new Browser(new MyBootstrapper());
            
            [SetUp]
            public void Init()
            {
                var db = new ParcelsManagementDBContext();
                db.Database.Delete();
            }

            [Test]
            public void CanRegister()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register two operators and check their existance (exception will
                // be thrown, if they do not exist)
                var registerResponse1 = browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op1 = db.Operators.First(c => c.Email == "foo1@bar.ru");
                var deserealizedRegisterResponse1 = Json.Decode(registerResponse1.Result.Body.AsString());
                
                var registerResponse2 = browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "bar123bar");
                    with.HttpRequest();
                });
                var op2 = db.Operators.First(c => c.Email == "foo2@bar.ru");
                var deserealizedRegisterResponse2 = Json.Decode(registerResponse2.Result.Body.AsString());
                
                // Compare their passwords' hashes to what we have entered
                Assert.AreEqual(op1.Password, ServerUtilities.GetHash("foo123foo"));
                Assert.AreEqual(op2.Password, ServerUtilities.GetHash("bar123bar"));
                
                // Compare returned tokens with ones in DB
                Assert.AreEqual(op1.Token, deserealizedRegisterResponse1["result"]["token"]);
                Assert.AreEqual(op2.Token, deserealizedRegisterResponse2["result"]["token"]);
            }

            [Test]
            public void CanLogIn()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register operator
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in as this operator
                var logInResponse = browser.Get("/operator/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var deserealizedLogInResponse = Json.Decode(logInResponse.Result.Body.AsString());
                Assert.AreEqual(deserealizedLogInResponse["status"], "ok");
                
                // Check returned token
                var op = db.Operators.First(c => c.Email == "foo1@bar.ru");
                var result = deserealizedLogInResponse["result"];
                Assert.AreEqual(result["token"], op.Token);
            }

            [Test]
            public void CanLogOut()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register operator
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in and check token exists
                browser.Get("/operator/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var op = db.Operators.First(c => c.Email == "foo1@bar.ru");
                Assert.NotNull(op.Token);

                // End operator session and check token was nullified
                browser.Put("/operator/log/out", with =>
                {
                    with.FormValue("token", op.Token);
                    with.HttpRequest();
                });
                db.Entry(op).Reload();
                Assert.IsNull(op.Token);
            }

            [Test]
            public void CanConfirmParcelAndGetUnconfirmed()
            {
                var db = new ParcelsManagementDBContext();
                
                // Create two parcels without confirming them
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Gnomes");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                
                // Create operator and get his all new parcels; they're automatically assigned to him
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op = db.Operators.First(o => o.Email == "foo1@bar.ru");
                var getUnconfirmedParcelsResponse = browser.Get("/operator/get/new/deliveries", with =>
                {
                    with.Query("token", op.Token);
                    with.HttpRequest();
                });
                var desGetUnconfirmedParcelsResponse = 
                    Json.Decode(getUnconfirmedParcelsResponse.Result.Body.AsString());

                // Check there are both parcels in response
                Assert.AreEqual("ok", desGetUnconfirmedParcelsResponse["status"]);
                Assert.AreEqual("Toys", desGetUnconfirmedParcelsResponse["result"][0]["name"]);
                Assert.AreEqual("Gnomes", desGetUnconfirmedParcelsResponse["result"][1]["name"]);
                
                // Confirm one of the parcels and check get all new parcels
                // request and status of the confirmed one
                var parcelId = db.Parcels.First(p => p.Name == "Toys").Id;
                browser.Put("/operator/confirm/delivery", with =>
                {
                    with.FormValue("token", op.Token);
                    with.FormValue("id", parcelId.ToString());
                    with.HttpRequest();
                });
                getUnconfirmedParcelsResponse = browser.Get("/operator/get/new/deliveries", with =>
                {
                    with.Query("token", op.Token);
                    with.HttpRequest();
                });
                desGetUnconfirmedParcelsResponse = 
                    Json.Decode(getUnconfirmedParcelsResponse.Result.Body.AsString());
                
                Assert.AreEqual("ok", desGetUnconfirmedParcelsResponse["status"]);
                Assert.AreEqual("Gnomes", desGetUnconfirmedParcelsResponse["result"][0]["name"]);
                // Assert.IsTrue(db.Parcels.Single(p => p.Id == parcelId).IsConfirmed);
            }

            [Test]
            public void CanManuallyAssignParcels()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register operator and a parcel. Parcel was assigned to him
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                var parcel = db.Parcels.Single(p => p.Name == "Toys");
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op1 = db.Operators.First(o => o.Email == "foo1@bar.ru");
                
                // Register another operator and reassign parcel to him
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op2 = db.Operators.First(o => o.Email == "foo2@bar.ru");
                browser.Put("/operator/assign/parcel", with =>
                {
                    with.FormValue("token", op2.Token);
                    with.FormValue("id", parcel.Id.ToString());
                    with.HttpRequest();
                });

                // Check it was reassigned and first operator hasn't got it in the list
                db.Entry(op1).Reload();
                db.Entry(op2).Reload();
                db.Entry(parcel).Reload();

                Assert.IsTrue(parcel.Operator.Id == op2.Id);
                Assert.IsTrue(op2.Parcels.Any(p => p.Id == parcel.Id));
                Assert.IsFalse(op1.Parcels.Any());
            }

            [Test]
            public void CanGetAutomaticallyAssignedParcels()
            {
                var db = new ParcelsManagementDBContext();
                
                // Create parcel without registering operator
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                
                // Register operator and check, if parcel was assigned to him
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op1 = db.Operators.First(o => o.Email == "foo1@bar.ru");
                var getParcelsResponse = browser.Get("/operator/get/deliveries", with =>
                {
                    with.Query("token", op1.Token);
                    with.HttpRequest();
                });
                var desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Toys", desGetParcelsResponse["result"][0]["name"]);

                // Create another parcel and check, if it was assigned to him
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Baloons");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/operator/get/deliveries", with =>
                {
                    with.Query("token", op1.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Baloons", desGetParcelsResponse["result"][1]["name"]);

                // Register another operator and parcel. It must be assigned to the second operator
                browser.Put("/operator/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op2 = db.Operators.First(o => o.Email == "foo2@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Barrels");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/operator/get/deliveries", with =>
                {
                    with.Query("token", op2.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Barrels", desGetParcelsResponse["result"][0]["name"]);

                // Log out the second operator and create parcel. Although first operator has
                // less parcels, this one will be assigned to him, as the second is offline
                browser.Put("/operator/log/out", with =>
                {
                    with.FormValue("token", op2.Token);
                    with.HttpRequest();
                });
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Gnomes");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/operator/get/deliveries", with =>
                {
                    with.Query("token", op1.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Gnomes", desGetParcelsResponse["result"][2]["name"]);
            }
        }

        [TestFixture]
        public class DriverAPITests
        {       
            private readonly Browser browser = new Browser(new MyBootstrapper());
            
            [SetUp]
            public void Init()
            {
                var db = new ParcelsManagementDBContext();
                db.Database.Delete();
            }
            
            [Test]
            public void CanRegister()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register two drivers and check their existance (exception will
                // be thrown, if they do not exist)
                var registerResponse1 = browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var driver1 = db.Drivers.First(d => d.Email == "foo1@bar.ru");
                var deserealizedRegisterResponse1 = Json.Decode(registerResponse1.Result.Body.AsString());
                
                var registerResponse2 = browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "bar123bar");
                    with.HttpRequest();
                });
                var driver2 = db.Drivers.First(d => d.Email == "foo2@bar.ru");
                var deserealizedRegisterResponse2 = Json.Decode(registerResponse2.Result.Body.AsString());
                
                // Check their passwords' hashes to what we have entered
                Assert.AreEqual(driver1.Password, ServerUtilities.GetHash("foo123foo"));
                Assert.AreEqual(driver2.Password, ServerUtilities.GetHash("bar123bar"));
                
                // Compare returned tokens with ones in DB
                Assert.AreEqual(driver1.Token, deserealizedRegisterResponse1["result"]["token"]);
                Assert.AreEqual(driver2.Token, deserealizedRegisterResponse2["result"]["token"]);
            }
            
            [Test]
            public void CanLogIn()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register operator
                browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in as this driver
                var logInResponse = browser.Get("/driver/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var deserealizedLogInResponse = Json.Decode(logInResponse.Result.Body.AsString());
                Assert.AreEqual(deserealizedLogInResponse["status"], "ok");
                
                // Check returned token
                var driver = db.Drivers.First(c => c.Email == "foo1@bar.ru");
                var result = deserealizedLogInResponse["result"];
                Assert.AreEqual(result["token"], driver.Token);
            }

            [Test]
            public void CanLogOut()
            {
                var db = new ParcelsManagementDBContext();
                
                // Register driver
                browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                
                // Log in and check token exists
                browser.Get("/driver/sign/in", with =>
                {
                    with.Query("email", "foo1@bar.ru");
                    with.Query("password", "foo123foo");
                });
                var driver = db.Drivers.First(c => c.Email == "foo1@bar.ru");
                Assert.NotNull(driver.Token);

                // End operator session and check token was nullified
                browser.Put("/driver/log/out", with =>
                {
                    with.FormValue("token", driver.Token);
                    with.HttpRequest();
                });
                db.Entry(driver).Reload();
                Assert.IsNull(driver.Token);
            }

            [Test]
            public void CanGetAssignedParcels()
            {
                var db = new ParcelsManagementDBContext();
                
                // Create parcel without registering operator
                browser.Put("/customer/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("phone", "+79991234567");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var cust = db.Customers.First(c => c.Email == "foo1@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Toys");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                
                // Register driver and check, if parcel was assigned to him
                browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo1@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var driver1 = db.Drivers.First(o => o.Email == "foo1@bar.ru");
                Console.WriteLine(driver1.Token);
                var getParcelsResponse = browser.Get("/driver/get/deliveries", with =>
                {
                    with.Query("token", driver1.Token);
                    with.HttpRequest();
                });
                var desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Toys", desGetParcelsResponse["result"][0]["name"]);

                // Create another parcel and check, if it was assigned to him
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Baloons");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/driver/get/deliveries", with =>
                {
                    with.Query("token", driver1.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Baloons", desGetParcelsResponse["result"][1]["name"]);

                // Register another driver and parcel. It must be assigned to the second driver
                browser.Put("/driver/register", with =>
                {
                    with.FormValue("email", "foo2@bar.ru");
                    with.FormValue("name", "Nickolay");
                    with.FormValue("password", "foo123foo");
                    with.HttpRequest();
                });
                var op2 = db.Drivers.First(o => o.Email == "foo2@bar.ru");
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Barrels");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/driver/get/deliveries", with =>
                {
                    with.Query("token", op2.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Barrels", desGetParcelsResponse["result"][0]["name"]);

                // Log out the second driver and create parcel. Although first driver has
                // less parcels, this one will be assigned to him, as the second is offline
                browser.Put("/driver/log/out", with =>
                {
                    with.FormValue("token", op2.Token);
                    with.HttpRequest();
                });
                browser.Put("/customer/create/delivery", with =>
                {
                    with.FormValue("token", cust.Token);
                    with.FormValue("del_name", "Gnomes");
                    with.FormValue("source", "Kolotushkina str");
                    with.FormValue("dest", "Pushkina str");
                    with.FormValue("urgency", "low");
                    with.FormValue("type", "usual");
                    with.HttpRequest();
                });
                getParcelsResponse = browser.Get("/driver/get/deliveries", with =>
                {
                    with.Query("token", driver1.Token);
                    with.HttpRequest();
                });
                desGetParcelsResponse = Json.Decode(getParcelsResponse.Result.Body.AsString());
                Assert.AreEqual("ok", desGetParcelsResponse["status"]);
                Assert.AreEqual("Gnomes", desGetParcelsResponse["result"][2]["name"]);
            }
        }
    }
}