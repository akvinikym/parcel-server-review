﻿using System.Collections.Generic;
using System.Linq;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class DatabaseTests
    {
        [SetUp]
        public void Init()
        {
            // Drop the previous test DB
            var db = new ParcelsManagementDBContext();
            db.Database.Delete();
            db.SaveChanges();
        }

        [TearDown]
        public void Dispose()
        {
            
        }

        [Test]
        public void CanInteractWithDB()
        {
            var db = new ParcelsManagementDBContext();
            
            // Add new customer to db
            var cust = db.Customers.Add(new Customer()
            {
                Id = db.Customers.Count() + 1,
                Name = "Foo",
                Email = "1@2.mier",
                Password = "foo123foo"
            });
            db.SaveChanges();

            // Check, if he was added
            Assert.AreEqual(
                db.Customers.First(c => c.Id == cust.Id).Email, cust.Email);

            // Change his name and check, if it was changed
            db.Customers.First(c => c.Id == cust.Id).Name = "Bar";
            db.SaveChanges();
            Assert.AreEqual(
                db.Customers.First(c => c.Id == cust.Id).Name, "Bar");
            
            // Remove him from db and check, if he was deleted
            db.Customers.Remove(
                db.Customers.First(c => c.Id == cust.Id));
            db.SaveChanges();
            Assert.IsNull(
                db.Customers.FirstOrDefault(c => c.Id == cust.Id));
        }
        
        [Test]
        public void ParcelCustomerFKWorks()
        {
            var db = new ParcelsManagementDBContext();
            
            // Add new customers to db
            var cust1 = db.Customers.Add(new Customer()
            {
                Id = db.Customers.Count() + 1,
                Name = "Foo",
                Email = "1@1.mier",
                Password = "foo123foo"
            });
            var cust2 = db.Customers.Add(new Customer()
            {
                Id = db.Customers.Count() + 2,
                Name = "Foo",
                Email = "1@2.mier",
                Password = "foo123foo"
            });
            
            // Add new parcel to db with created customer as owner
            var parcel = db.Parcels.Add(new Parcel()
            {
                Id = db.Parcels.Count() + 1,
                Name = "Toys",
                SourceAddress = "Pushkina str",
                DestinationAddress = "Kolotushkina str",
                Urgency = "high",
                Type = "usual",
                Customer = cust1
            });
            db.SaveChanges();

            // Fetch them from the DB
            var custFromDB = db.Customers.First(c => c.Id == cust1.Id);
            var cust2FromDB = db.Customers.First(c => c.Id == cust2.Id);
            var parcelFromDB = db.Parcels.First(p => p.Id == parcel.Id);

            // Check the both-side connection
            Assert.IsTrue(custFromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(custFromDB, parcelFromDB.Customer);
            
            // Change the customer of parcel to another one
            parcelFromDB.Customer = cust2FromDB;
            db.SaveChanges();
            
            // Check the updated both-side connection
            Assert.IsFalse(custFromDB.Parcels.Contains(parcelFromDB));
            Assert.IsTrue(cust2FromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(cust2FromDB, parcelFromDB.Customer);
            
            // Remove them from DB
            db.Parcels.Remove(parcelFromDB);
            db.Customers.Remove(custFromDB);
            db.Customers.Remove(cust2FromDB);
            db.SaveChanges();
        }
        
        [Test]
        public void ParcelOperatorFKWorks()
        {
            var db = new ParcelsManagementDBContext();
            
            // Add new customer and operators
            var cust = db.Customers.Add(new Customer()
            {
                Id = db.Customers.Count() + 1,
                Name = "Foo",
                Email = "1@1.mier",
                Password = "foo123foo"
            });
            var op1 = db.Operators.Add(new Operator()
            {
                Id = db.Operators.Count() + 1,
                Name = "Foo",
                Email = "1@1.mier",
                Password = "foo123foo"
            });
            var op2 = db.Operators.Add(new Operator()
            {
                Id = db.Operators.Count() + 2,
                Name = "Foo",
                Email = "1@2.mier",
                Password = "foo123foo"
            });
            
            // Add new parcel to db with created op as assigned
            var parcel = db.Parcels.Add(new Parcel()
            {
                Id = db.Parcels.Count() + 1,
                Name = "Toys",
                SourceAddress = "Pushkina str",
                DestinationAddress = "Kolotushkina str",
                Urgency = "high",
                Type = "usual",
                Customer = cust,
                Operator = op1
            });
            db.SaveChanges();

            // Fetch them from the DB
            var op1FromDB = db.Operators.First(e => e.Id == op1.Id);
            var op2FromDB = db.Operators.First(e => e.Id == op2.Id);
            var parcelFromDB = db.Parcels.First(p => p.Id == parcel.Id);

            // Check the both-side connection
            Assert.IsTrue(op1FromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(op1FromDB, parcelFromDB.Operator);
            
            // Change the customer of parcel to another one
            parcelFromDB.Operator = op2FromDB;
            db.SaveChanges();
            
            // Check the updated both-side connection
            Assert.IsFalse(op1FromDB.Parcels.Contains(parcelFromDB));
            Assert.IsTrue(op2FromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(op2FromDB, parcelFromDB.Operator);
            
            // Remove them from DB
            db.Parcels.Remove(parcelFromDB);
            db.Customers.Remove(cust);
            db.Operators.Remove(op1FromDB);
            db.Operators.Remove(op2FromDB);
            db.SaveChanges();
        }
        
        [Test]
        public void ParcelDriverFKWorks()
        {
            var db = new ParcelsManagementDBContext();
            
            // Add new customer and operators
            var cust = db.Customers.Add(new Customer()
            {
                Id = db.Customers.Count() + 1,
                Name = "Foo",
                Email = "1@1.mier",                
                Password = "foo123foo"
            });
            var dr1 = db.Drivers.Add(new Driver()
            {
                Id = db.Operators.Count() + 1,
                Name = "Foo",
                Email = "1@1.mier",                
                Password = "foo123foo"
            });
            var dr2 = db.Drivers.Add(new Driver()
            {
                Id = db.Operators.Count() + 2,
                Name = "Foo",
                Email = "1@2.mier",                
                Password = "foo123foo"
            });
            
            // Add new parcel to db with created driver as assigned
            var parcel = db.Parcels.Add(new Parcel()
            {
                Id = db.Parcels.Count() + 1,
                Name = "Toys",
                SourceAddress = "Pushkina str",
                DestinationAddress = "Kolotushkina str",
                Urgency = "high",
                Type = "usual",
                Customer = cust,
                Driver = dr1
            });
            db.SaveChanges();

            // Fetch them from the DB
            var dr1FromDB = db.Drivers.First(d => d.Id == dr1.Id);
            var dr2FromDB = db.Drivers.First(d => d.Id == dr2.Id);
            var parcelFromDB = db.Parcels.First(p => p.Id == parcel.Id);

            // Check the both-side connection
            Assert.IsTrue(dr1FromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(dr1FromDB, parcelFromDB.Driver);
            
            // Change the customer of parcel to another one
            parcelFromDB.Driver = dr2FromDB;
            db.SaveChanges();
            
            // Check the updated both-side connection
            Assert.IsFalse(dr1FromDB.Parcels.Contains(parcelFromDB));
            Assert.IsTrue(dr2FromDB.Parcels.Contains(parcelFromDB));
            Assert.AreEqual(dr2FromDB, parcelFromDB.Driver);
            
            // Remove them from DB
            db.Parcels.Remove(parcelFromDB);
            db.Customers.Remove(cust);
            db.Drivers.Remove(dr1FromDB);
            db.Drivers.Remove(dr2FromDB);
            db.SaveChanges();
        }
    }
}