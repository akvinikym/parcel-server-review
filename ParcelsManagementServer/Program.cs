﻿using ParcelsManagementServer.Server;

namespace ParcelsManagementServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Server.Server.Init();
        }
    }
}