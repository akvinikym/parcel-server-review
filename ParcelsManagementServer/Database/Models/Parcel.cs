﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ParcelsManagementServer.Database.Models
{
    public class Parcel
    {
        public enum ParcelStatus
        {
            Created,                // Created, not confirmed by anyone
            ConfirmedByOperator,
            ConfirmedByDriver,
            PickedUp,               // Driver picked it up
            Delivered,              // Driver delivered it; waiting for assessment
            AssessedByCustomer,     // Customer assessed the parcel
            Archived,               // Parcel was assessed by Manager and archived
            Rejected                // Parcel was rejected by the Operator
        }
        
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        [Required]
        public string Urgency { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public float Weight { get; set; }
        
        [Required]
        public ParcelStatus Status { get; set; }
        
        [Required]
        public string SourceAddress { get; set; }
        public string SourceAddressExtra { get; set; }
        [Required]
        public string SourceLatitude { get; set; }
        [Required]
        public string SourceLongitude { get; set; }
        [Required]
        public string DestinationAddress { get; set; }
        public string DestinationAddressExtra { get; set; }
        [Required]
        public string DestinationLatitude { get; set; }
        [Required]
        public string DestinationLongitude { get; set; }
        
        [Required]
        public int ConfirmationNumber { get; set; }
        
        public bool IsRejected { get; set; }
        public string RejectMessageForCustomer { get; set; }
        public string RejectMessageForEmployees { get; set; }
        
        public int ArchiveEntryId { get; set; }
        
        public int CustomerId { get; set; }
        public int? OperatorId { get; set; }
        public int? DriverId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Operator Operator { get; set; }
        public virtual Driver Driver { get; set; }
        public virtual ParcelAssessor ParcelAssessor { get; set; }
    }
}