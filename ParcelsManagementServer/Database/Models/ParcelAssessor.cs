﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ParcelsManagementServer.Database.Models
{
    public class ParcelAssessor
    {
        public int ParcelId { get; set; }
        
        // Time-related properties; one per each status change
        public DateTime TimeCreated { get; set; }
        public DateTime TimeConfirmedByOperator { get; set; }
        public DateTime TimeConfirmedByDriver { get; set; }
        public DateTime TimePickedUp { get; set; }
        public DateTime TimeDelivered { get; set; }
        public DateTime TimeAssessedByCustomer { get; set; }
        public DateTime TimeAssessedByManager { get; set; }
        public DateTime TimeRejected { get; set; }
        
        // Assessments
        public float ManagerMark { get; set; }
        public string ManagerComment { get; set; }
        public float CustomerMark { get; set; }
        public string CustomerComment { get; set; }
        
        public int? ManagerId { get; set; }
        public int? CustomerId { get; set; }
        
        public virtual Manager Manager { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Parcel Parcel { get; set; }
    }
}