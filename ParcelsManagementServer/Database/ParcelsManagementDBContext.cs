﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.Controllers;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Database
{
    public class ParcelsManagementDBContext : DbContext
    {
        public ParcelsManagementDBContext() : base("parcels")
        {
            System.Data.Entity.Database.SetInitializer(
                new DropCreateDatabaseIfModelChanges<ParcelsManagementDBContext>());

            // Insert default users for some roles
            if (!this.Overseers.Any())
            {
                this.Overseers.Add(new Overseer()
                {
                    Id = 1,
                    Email = "super@dooper.com",
                    Name = "The One",
                    Password = ServerUtilities.GetHash("super")
                });
                this.Managers.Add(new Manager()
                {
                    Email = "manager@mail.com",
                    Name = "DefaultManager",
                    Password = ServerUtilities.GetHash("manager")
                });
                this.Operators.Add(new Operator()
                {
                    Email = "op@mail.com",
                    Name = "DefaultOperator",
                    Password = ServerUtilities.GetHash("operator"),
                    Token = "w392fjed3jd932"
                });
                this.Drivers.Add(new Driver()
                {
                    Email = "dr@mail.com",
                    Name = "DefaultDriver",
                    Phone = "+79991234567",
                    Password = ServerUtilities.GetHash("driver")
                });
                
                
                this.SaveChanges();
            }
        }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            // Foreign key constraints creation
            mb.Entity<Parcel>()
                .HasRequired(p => p.Customer)
                .WithMany(c => c.Parcels)
                .HasForeignKey(p => p.CustomerId);
            mb.Entity<Parcel>()
                .HasOptional(p => p.Operator)
                .WithMany(o => o.Parcels)
                .HasForeignKey(p => p.OperatorId);
            mb.Entity<Parcel>()
                .HasOptional(p => p.Driver)
                .WithMany(d => d.Parcels)
                .HasForeignKey(p => p.DriverId);

            mb.Entity<ParcelAssessor>()
                .HasKey(pa => pa.ParcelId);
            mb.Entity<Parcel>()
                .HasRequired(p => p.ParcelAssessor)
                .WithRequiredPrincipal(pa => pa.Parcel);

            mb.Entity<ParcelAssessor>()
                .HasOptional(pa => pa.Manager)
                .WithMany(m => m.AssessedParcels)
                .HasForeignKey(pa => pa.ManagerId);
            mb.Entity<ParcelAssessor>()
                .HasOptional(pa => pa.Customer)
                .WithMany(p => p.AssesedParcels)
                .HasForeignKey(pa => pa.CustomerId);
        }
        
        /// <summary>
        /// Parcel with parameters
        /// </summary>
        public DbSet<Parcel> Parcels { get; set; }
        
        /// <summary>
        /// Customer of the system
        /// </summary>
        public DbSet<Customer> Customers { get; set; }
        
        /// <summary>
        /// Operator from the central office
        /// </summary>
        public DbSet<Operator> Operators { get; set; }
        
        /// <summary>
        /// Manager, who has access to almost all features and can register
        /// new operators and drivers
        /// </summary>
        public DbSet<Manager> Managers { get; set; }
        
        /// <summary>
        /// Main manager, who can register managers
        /// </summary>
        public DbSet<Overseer> Overseers { get; set; }
        
        /// <summary>
        /// Driver in the system
        /// </summary>
        public DbSet<Driver> Drivers { get; set; }
    }
}