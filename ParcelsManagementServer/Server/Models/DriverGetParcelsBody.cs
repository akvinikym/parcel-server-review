﻿namespace ParcelsManagementServer.Server.Models
{
    public class DriverGetParcelsBody
    {
        public int id { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public string urgency { get; set; }
        public string type { get; set; }
        public string status { get; set; }
    }
}