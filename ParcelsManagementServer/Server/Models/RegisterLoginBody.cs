﻿namespace ParcelsManagementServer.Server.Models
{
    public class RegisterLoginBody
    {
        public int id { get; set; }
        public string token { get; set; }
    }
}