﻿using System.Collections.Generic;

namespace ParcelsManagementServer.Server.Models
{
    public class CustomerGetParcelsBody
    {
        public int id { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public string urgency { get; set; }
        public string type { get; set; }
        public string status { get; set; }
        public string reject_msg { get; set; }
        public int conf_num { get; set; }
    }
}