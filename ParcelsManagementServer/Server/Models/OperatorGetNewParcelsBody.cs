﻿namespace ParcelsManagementServer.Server.Models
{
    public class OperatorGetNewParcelsBody
    {
        public int id { get; set; }
        public int operator_id { get; set; }
        public string name { get; set; }
        public string source { get; set; }
        public string src_lat { get; set; }
        public string src_lon { get; set; }
        public string destination { get; set; }
        public string dest_lat { get; set; }
        public string dest_lon {get; set; }
        public string urgency { get; set; }
        public string type { get; set; }
        public string status { get; set; }
    }
}