﻿using System.Collections.Generic;

namespace ParcelsManagementServer.Server.Models
{
    /// <summary>
    /// Response schema for requests
    /// </summary>
    public class ServerResponse
    {
        // Status strings
        public const string SuccessfullResponse = "ok";
        public const string InternalError = "int_error";
        public const string ExternalError = "ext_service_error";
        public const string NoEmail = "no_email";
        public const string WrongPassword = "wrong_pswd";
        public const string WrongOperator = "wrong_op";
        public const string WrongCustomer = "wrong_cust";
        public const string WrongDriver = "wrong_driver";
        public const string WrongToken = "wrong_tok";
        public const string WrongConfNumber = "wrong_conf_num";
        public const string NoParcel = "no_parcel";
        public const string UserAlreadyRegistered = "registered_alr";
        public const string NoOperator = "no_op_with_id";
        public const string NoDriver = "no_dr_with_id";
        public const string ParcelNotDeliveredOrRejected = "parcel_not_delivered_or_rejected";
        public const string WrongStatusChangeAttempt = "wrong_status_change";
        public const string NoOperatorsOnline = "no_op_online";
        
        public string status { get; set; }
        public object result { get; set; }
    }
}