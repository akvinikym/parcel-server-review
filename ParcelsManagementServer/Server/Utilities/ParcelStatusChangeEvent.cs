﻿using System;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;

namespace ParcelsManagementServer.Server.Utilities
{
    /// <summary>
    /// Publisher for parcel status change event
    /// </summary>
    public class ParcelStatusChangeEvent
    {
        public class ParcelStatusChangeEventArgs : EventArgs
        {
            public readonly int ParcelId;
            public ParcelStatusChangeEventArgs(int id)
            {
                ParcelId = id;
            }
        }

        public event EventHandler<ParcelStatusChangeEventArgs> StatusChangeEvent;

        public void StatusChanged(int parcelId)
        {
            OnRaiseStatusChange(new ParcelStatusChangeEventArgs(parcelId));
        }

        private void OnRaiseStatusChange(ParcelStatusChangeEventArgs args)
        {
            StatusChangeEvent?.Invoke(this, args);
        }
        
        /// <summary>
        /// Change status of the parcel, if possible
        /// </summary>
        public static bool ChangeParcelStatus(Parcel.ParcelStatus newStatus, int parcelId)
        {
            using (var db = new ParcelsManagementDBContext())
            {
                var parcel = db.Parcels.Find(parcelId);
                var currentStatus = parcel.Status;
                if (newStatus == Parcel.ParcelStatus.Created)
                    return false;
                
                switch (currentStatus)
                {
                    case Parcel.ParcelStatus.Created:
                    {
                        if (newStatus != Parcel.ParcelStatus.ConfirmedByOperator &&
                            newStatus != Parcel.ParcelStatus.Rejected)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.ConfirmedByOperator:
                    {
                        if (newStatus != Parcel.ParcelStatus.ConfirmedByDriver)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.ConfirmedByDriver:
                    {
                        if (newStatus != Parcel.ParcelStatus.PickedUp)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.PickedUp:
                    {
                        if (newStatus != Parcel.ParcelStatus.Delivered)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.Delivered:
                    {
                        if (newStatus != Parcel.ParcelStatus.AssessedByCustomer)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.AssessedByCustomer:
                    {
                        if (newStatus != Parcel.ParcelStatus.Archived)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.Rejected:
                    {
                        if (newStatus != Parcel.ParcelStatus.Archived)
                            return false;
                        break;
                    }
                    case Parcel.ParcelStatus.Archived:
                    {
                        return false;
                    }
                    default:
                    {
                        return false;
                    }
                }

                parcel.Status = newStatus;
                db.SaveChanges();
                ServerUtilities.ParcelStatusChanged(parcelId);

                return true;
            }
        }
    }
}