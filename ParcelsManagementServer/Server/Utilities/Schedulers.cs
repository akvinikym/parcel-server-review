﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;

namespace ParcelsManagementServer.Server.Utilities
{
    /// <summary>
    /// Methods, which helps to assign parcels to various employee roles,
    /// depending on their special conditons and parameters
    /// </summary>
    public static class Schedulers
    {
        private static readonly Random rand = new Random();
        
        /// <summary>
        /// Choose operator for the next parcel. Returns -1, if no suitable operator is found
        /// </summary>
        public static int ChooseOperator()
        {
            using (var db = new ParcelsManagementDBContext())
            {
                // If there are no registered operators, leave it as-is,
                // operator will be assigned this parcel later
                if (!db.Operators.Any())
                    return -1;

                // For now, we consider only load of operators and
                // assign parcels to the least loaded ones
                var leastLoad = int.MaxValue;
                var leastLoadedOperators = new List<Operator>();
                foreach (var op in db.Operators)
                {
                    // If operator is offline, we cannot assign him the parcel
                    if (op.Token == null) continue;
                    
                    if (leastLoad > op.Parcels.Count)
                    {
                        leastLoad = op.Parcels.Count;
                        leastLoadedOperators.Clear();
                        leastLoadedOperators.Add(op);
                    }
                    else if (leastLoad == op.Parcels.Count)
                    {
                        leastLoadedOperators.Add(op);
                    }
                }
                // TODO: FIX THIS BAG!!!
                return leastLoadedOperators.Count == 0 
                    ? -1
                    : leastLoadedOperators[rand.Next(0, leastLoadedOperators.Count)].Id;
            }
        }
    }
}