﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Flurl;
using Flurl.Http;
using ParcelsManagementServer.Server.InterserviceCommunicators;

namespace ParcelsManagementServer.Server.Utilities
{
    /// <summary>
    /// Various utilities, which are used inside the server and serve different purposes
    /// </summary>
    public static class ServerUtilities
    {
        /// <summary>
        /// Convert the following object into json string
        /// </summary>
        public static string ToJson(object str)
        {
            return new JavaScriptSerializer().Serialize(str);
        }

        /// <summary>
        /// Get hash of the string
        /// </summary>
        public static string GetHash(string s)
        {
            const string salt = "588Ii84t5Ety5A0";

            return Convert.ToBase64String(
                new SHA256CryptoServiceProvider().ComputeHash(
                    Encoding.UTF8.GetBytes(s + salt)));
        }

        private const string TokenAlphabet = 
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private const int TokenLength = 15;
        /// <summary>
        /// Get a new random token
        /// </summary>
        public static string GetNewToken()
        { 
            var randomNumber = new byte[1];
            var rand = new RNGCryptoServiceProvider();

            var token = new char[TokenLength];
            for (var i = 0; i < TokenLength; i++)
            {
                rand.GetBytes(randomNumber);
                var nextCharPos = Convert.ToInt32(randomNumber[0]) % TokenAlphabet.Length;
                token[i] = TokenAlphabet[nextCharPos];
            }

            return new string(token);
        }
        
        private const string Numbers = "1234567890";
        public const int CustomerConfirmationNumberLength = 4;
        /// <summary>
        /// Get a secure-way constructed random number of a given length
        /// </summary>
        public static int GetNewRandomNumber(int length)
        {
            var randomNumber = new byte[1];
            var rand = new RNGCryptoServiceProvider();

            var outNumber = new char[length];
            for (var i = 0; i < length; i++)
            {
                rand.GetBytes(randomNumber);
                var nextNumPos = Convert.ToInt32(randomNumber[0]) % Numbers.Length;
                outNumber[i] = Numbers[nextNumPos];
            }

            return Convert.ToInt32(new string(outNumber));
        }

        /// <summary>
        /// Post a HTTP request. Returns its status code and stringified body of response
        /// </summary>
        public static async Task<Tuple<HttpStatusCode, string>> PostHttpJsonRequest(
            string address, object jsonObject)
        {
            var url = new Url(address);
            var response = await url.AllowAnyHttpStatus().PostJsonAsync(jsonObject);
    
            return new Tuple<HttpStatusCode, string>(
                response.StatusCode, response.Content.ReadAsStringAsync().Result);
        }

        public static long GetUnixEpoch(this DateTime dateTime)
        {
            return Convert.ToInt64((dateTime.ToUniversalTime() - 
                    new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
        }

        private static ParcelStatusChangeEvent _parcelStatusChangeEvent;
        /// <summary>
        /// Used on server init; configures all the necessary events, which can happen in server
        /// </summary>
        public static void ConfigureServerEvents()
        {
            _parcelStatusChangeEvent = new ParcelStatusChangeEvent();
            _parcelStatusChangeEvent.StatusChangeEvent += NotificationServiceCommunicator.HandleStatusChange;
        }
        /// <summary>
        /// Called whenever status of the parcel changes; incapsulates the event system
        /// </summary>
        public static void ParcelStatusChanged(int parcelId)
        {
            _parcelStatusChangeEvent.StatusChanged(parcelId);
        }
    }
}