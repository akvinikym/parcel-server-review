﻿using System;
using System.Linq;
using Nancy;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server.Controllers
{
    public class MapServiceController : NancyModule
    {
        private const string MapServiceToken = "feewSJ39fdRdj3JK";
        
        public MapServiceController()
        {
            Get["/maps/driver/id/{get}"] = async (parameters, tok) =>
            {
                var drToken = Request.Query["driver_token"];
                var serviceToken = Request.Query["service_token"];
                return GetDriverIdByToken(drToken, serviceToken);
            };
        }

        private static string GetDriverIdByToken(string drToken, string serviceToken)
        {
            var response = new ServerResponse();
            if (serviceToken != MapServiceToken)
            {
                response.status = ServerResponse.WrongToken;
                return ServerUtilities.ToJson(response);
            }
            
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.SingleOrDefault(d => d.Token == drToken);
                    if (driver == null)
                    {
                        response.status = ServerResponse.NoDriver;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = driver.Id;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
    }
}