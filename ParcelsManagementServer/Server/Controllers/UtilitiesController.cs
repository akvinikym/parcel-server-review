﻿using Nancy;
using ParcelsManagementServer.Database;

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Handles '/' HTTP request, which is used to ping server
    /// </summary>
    public class UtilitiesController : NancyModule
    {
        public UtilitiesController()
        {
            Get["/"] = async (parameters, tok) => "Welcome to Server API!";
            
            Get["/db/{drop}"] = async (parameters, tok) =>
            {
                var db = new ParcelsManagementDBContext();
                db.Database.Delete();
                db.SaveChanges();

                return "DB is dropped";
            };
        }
    }
}