﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;
#pragma warning disable 1998

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Overseer requests
    /// </summary>
    public class OverseerController : NancyModule
    {
        public OverseerController()
        {
            Get["/overseer/sign/{in}"] = async (parameters, tok) =>
            {
                var email = Request.Query["email"];
                var pswd = Request.Query["password"];
                return SignIn(email, pswd);
            };

            Put["/overseer/log/{out}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                return LogOut(token);
            };
            
            Put["/overseer/manager/new/{register}"] = async (parameters, tok) =>
            {
                var email = Request.Form["email"];
                var name = Request.Form["name"];
                var pswd = Request.Form["password"];
                var token = Request.Form["token"];
                return RegisterManager(email, name, pswd, token);
            };

            Get["/overseer/manager/all/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetAllManagers(token);
            };
        }
        
        private static string SignIn(
            string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var overseer = db.Overseers.FirstOrDefault(o => o.Email == email);
                    if (overseer == null)
                    {
                        response.status = ServerResponse.NoEmail;
                        return ServerUtilities.ToJson(response);
                    }
                    if (overseer.Password != ServerUtilities.GetHash(password))
                    {
                        response.status = ServerResponse.WrongPassword;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var tok = ServerUtilities.GetNewToken();
                    overseer.Token = tok;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = overseer.Id,
                        token = tok
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string LogOut(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var overseer = db.Overseers.FirstOrDefault(o => o.Token == token);
                    if (overseer == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    overseer.Token = null;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string RegisterManager(
            string email, string name, string password, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(e => e.Email == email) != null)
                    {
                        response.status = ServerResponse.UserAlreadyRegistered;
                        return ServerUtilities.ToJson(response);
                    }
                    if (db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var manager = db.Managers.Add(new Manager()
                    {
                        Name = name,
                        Email = email,
                        Password = ServerUtilities.GetHash(password)
                    });
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = manager.Id
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string GetAllManagers(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var managers = new List<object>();
                    foreach (var manager in db.Managers)
                    {
                        managers.Add(new
                        {
                            id = manager.Id,
                            name = manager.Name,
                            email = manager.Email,
                            is_online = manager.Token != null
                        });
                    }
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = managers;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
    }
}