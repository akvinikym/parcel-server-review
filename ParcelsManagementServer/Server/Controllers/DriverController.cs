﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.InterserviceCommunicators;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;

#pragma warning disable 1998

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Driver's requests go here
    /// </summary>
    public class DriverController : NancyModule
    {
        public DriverController()
        {
            Get["/driver/sign/{in}"] = async (parameters, tok) =>
            {
                var email = Request.Query["email"];
                var pswd = Request.Query["password"];
                return SignIn(email, pswd);
            };
            
            Put["/driver/log/{out}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                return LogOut(token);
            };

            Get["/driver/get/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetDriversParcels(token);
            };
                        
            Put["/driver/confirm/{delivery}"] = async (parameters, tok) =>
            {
                var id = Request.Form["id"];
                var token = Request.Form["token"];
                return ConfirmParcel(id, token);
            };

            Put["/driver/pick_up/{delivery}"] = async (parameters, tok) =>
            {
                var id = Request.Form["id"];
                var token = Request.Form["token"];
                return PickUpParcel(id, token);
            };
            
            Put["/driver/complete/{delivery}"] = async (parameters, tok) =>
            {
                var id = Request.Form["id"];
                var token = Request.Form["token"];
                var confNum = Request.Form["conf_num"];
                return CompleteParcel(id, token, confNum);
            };
            
            Get["/driver/emergency/{button}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return SendEmergencySignal(token);
            };
        }

        private static string SignIn(
            string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(d => d.Email == email);
                    if (driver == null)
                    {
                        response.status = ServerResponse.NoEmail;
                        return ServerUtilities.ToJson(response);
                    }
                    if (driver.Password != ServerUtilities.GetHash(password))
                    {
                        response.status = ServerResponse.WrongPassword;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var tok = ServerUtilities.GetNewToken();
                    driver.Token = tok;
                    
                    var unassignedParcels = new List<Parcel>();
                    foreach (var parcel in db.Parcels.Where(d => d.Driver == null))
                    {
                        parcel.Driver = driver;
                        unassignedParcels.Add(parcel);
                    }
                    db.SaveChanges();
                    
                    foreach (var parcel in unassignedParcels)
                        SendNewParcelNotification(driver.Email, parcel.Id);

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = driver.Id,
                        token = tok
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string LogOut(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(d => d.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    driver.Token = null;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string GetDriversParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(e => e.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcelsFormatted = new List<object>();
                    foreach (var parcel in driver.Parcels)
                    {
                        parcelsFormatted.Add(new DriverGetParcelsBody()
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString()
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcelsFormatted;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string ConfirmParcel(int id, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(d => d.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = driver.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongDriver;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.ConfirmedByDriver, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.ParcelAssessor.TimeConfirmedByDriver = DateTime.Now;
                    db.SaveChanges();

                    SubscribeDriverToBroadcasts(parcel.Id);

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string PickUpParcel(int id, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(d => d.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = driver.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongDriver;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.PickedUp, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    parcel.ParcelAssessor.TimePickedUp = DateTime.Now;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string CompleteParcel(int id, string token, int confirmationNumber)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(d => d.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = driver.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongDriver;
                        return ServerUtilities.ToJson(response);
                    }

                    if (parcel.ConfirmationNumber != confirmationNumber)
                    {
                        response.status = ServerResponse.WrongConfNumber;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.Delivered, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    parcel.ParcelAssessor.TimeDelivered = DateTime.Now;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string SendEmergencySignal(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var driver = db.Drivers.FirstOrDefault(e => e.Token == token);
                    if (driver == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    // Find the first online operator from the parcels this driver carries. If there is no
                    // such operator, find the first one who is online
                    var op = (
                                 from parcel
                                     in driver.Parcels 
                                 where parcel.Operator.Token != null
                                 select parcel.Operator)
                             .FirstOrDefault() ?? (
                                 from _op
                                     in db.Operators
                                 where _op.Token != null
                                 select _op)
                             .FirstOrDefault();

                    if (op == null)
                    {
                        response.status = ServerResponse.NoOperatorsOnline;
                        return ServerUtilities.ToJson(response);
                    }

                    var signalIsSent = NotificationServiceCommunicator.SendEmergencyNotification(
                        driver.Id, driver.Phone, op.Email);
                    if (!signalIsSent)
                    {
                        response.status = ServerResponse.ExternalError;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        public static bool SendNewParcelNotification(
            string email, int id)
        {
            return NotificationServiceCommunicator.SendNewParcelNotificaton(
                NotificationServiceCommunicator.Role.Driver, email, id);
        }

        private static bool SubscribeDriverToBroadcasts(int parcelId)
        {
            return WeatherTrafficServiceCommunicator.SubscribeDriver(parcelId);
        }
    }
}