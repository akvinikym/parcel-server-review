﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web.Helpers;
using Nancy;
using Nancy.Routing;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.InterserviceCommunicators;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;

#pragma warning disable 1998

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Customer's information and parcels requests go here
    /// </summary>
    public class CustomerController : NancyModule
    {
        public CustomerController()
        { 
            Put["/customer/{register}"] = async (parameters, tok) =>
            {
                var name = Request.Form["name"];
                var phone = Request.Form["phone"];
                var email = Request.Form["email"];
                var pswd = Request.Form["password"];
                return Register(
                    name, phone, email, pswd);
            };
            
            Get["/customer/sign/{in}"] = async (parameters, tok) =>
            {
                var email = Request.Query["email"];
                var pswd = Request.Query["password"];
                return SignIn(email, pswd);
            };
            
            Put["/customer/log/{out}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                return LogOut(token);
            };
            
            Put["/customer/create/{delivery}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                var deliveryName = Request.Form["del_name"];
                var urgency = Request.Form["urgency"];
                var type = Request.Form["type"];
                var srcAddress = Request.Form["src_address"];
                var srcAddress2 = Request.Form["src_address_2"];
                var srcLat = Request.Form["src_latitude"];
                var srcLon = Request.Form["src_longitude"];
                var destAddress = Request.Form["dest_address"];
                var destAddress2 = Request.Form["dest_address_2"];
                var destLat = Request.Form["dest_latitude"];
                var destLon = Request.Form["dest_longitude"];
                return CreateParcel(
                    token, deliveryName, urgency, type,
                    srcAddress, srcAddress2, srcLat, srcLon,
                    destAddress, destAddress2, destLat, destLon);
            };
            
            Get["/customer/get/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetCurrentParcels(token);
            };
            
            Get["/customer/get/archived/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetCurrentParcels(token);
            };

            Get["/customer/confirmation_number/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                var pId = Request.Query["parcel_id"];
                return GetConfirmationNumber(token, pId);
            };
            
            Put["/customer/assess/{delivery}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                var mark = Request.Form["mark"];
                var comment = Request.Form["comment"];
                var pId = Request.Form["parcel_id"];
                return AssessParcel(token, pId, mark, comment);
            };
        }

        private static string Register(
            string name, string phone, string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Customers.FirstOrDefault(c => c.Email == email) != null)
                    {
                        response.status = ServerResponse.UserAlreadyRegistered;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var token = ServerUtilities.GetNewToken();
                    var customer = db.Customers.Add(new Customer()
                    {
                        Name = name,
                        Phone = phone,
                        Email = email,
                        Password = ServerUtilities.GetHash(password),
                        Token = token
                    });
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = customer.Id,
                        token = token
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string SignIn(
            string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var customer = db.Customers.FirstOrDefault(c => c.Email == email);
                    if (customer == null)
                    {
                        response.status = ServerResponse.NoEmail;
                        return ServerUtilities.ToJson(response);
                    }
                    if (customer.Password != ServerUtilities.GetHash(password))
                    {
                        response.status = ServerResponse.WrongPassword;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var tok = ServerUtilities.GetNewToken();
                    customer.Token = tok;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = customer.Id,
                        token = tok
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        
        private static string LogOut(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var customer = db.Customers.FirstOrDefault(c => c.Token == token);
                    if (customer == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    customer.Token = null;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string CreateParcel(
            string token, string delName, string urgency, string type,
            string srcAddress, string srcAddress2, string srcLat, string srcLon,
            string destAddress, string destAddress2, string destLat, string destLon)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var cust = db.Customers.FirstOrDefault(c => c.Token == token);
                    if (cust == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var assignedOpId = Schedulers.ChooseOperator();
                    var parcel = db.Parcels.Add(new Parcel()
                    {
                        Name = delName,
                        Urgency = urgency,
                        Type = type,
                        Status = Parcel.ParcelStatus.Created,
                        SourceAddress = srcAddress,
                        SourceAddressExtra = srcAddress2,
                        SourceLatitude = srcLat,
                        SourceLongitude = srcLon,
                        DestinationAddress = destAddress,
                        DestinationAddressExtra = destAddress2,
                        DestinationLatitude = destLat,
                        DestinationLongitude = destLon,
                        ConfirmationNumber = 
                            ServerUtilities.GetNewRandomNumber(ServerUtilities.CustomerConfirmationNumberLength),
                        Customer = cust,
                        Operator = assignedOpId != -1
                            ? db.Operators.Single(o => o.Id == assignedOpId)
                            : null
                    });
                    parcel.ParcelAssessor = new ParcelAssessor {TimeCreated = DateTime.Now};
                    db.SaveChanges();

                    if (assignedOpId != -1)
                    {
                        NotificationServiceCommunicator.SendNewParcelNotificaton(
                            NotificationServiceCommunicator.Role.Operator, db.Operators.Find(assignedOpId).Email, parcel.Id);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetCurrentParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var cust = db.Customers
                        .FirstOrDefault(c => c.Token == token);
                    if (cust == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcels = new List<CustomerGetParcelsBody>();
                    foreach (var parcel in cust.Parcels)
                    {
                        if (parcel.Status == Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new CustomerGetParcelsBody()
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString(),
                            reject_msg = parcel.RejectMessageForCustomer,
                            conf_num = parcel.ConfirmationNumber
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetArchivedParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var cust = db.Customers
                        .FirstOrDefault(c => c.Token == token);
                    if (cust == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcels = new List<CustomerGetParcelsBody>();
                    foreach (var parcel in cust.Parcels)
                    {
                        if (parcel.Status != Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new CustomerGetParcelsBody()
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString(),
                            reject_msg = parcel.RejectMessageForCustomer,
                            conf_num = parcel.ConfirmationNumber
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetConfirmationNumber(string token, int id)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var cust = db.Customers
                        .FirstOrDefault(c => c.Token == token);
                    if (cust == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcel = cust.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongCustomer;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcel.ConfirmationNumber;
                    
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string AssessParcel(string token, int id, float mark, string comment)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Customers.FirstOrDefault(c => c.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.AssessedByCustomer, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.ParcelAssessor.CustomerMark = mark;
                    parcel.ParcelAssessor.CustomerComment = comment;
                    parcel.ParcelAssessor.TimeAssessedByCustomer = DateTime.Now;
                    parcel.ParcelAssessor.Customer = db.Customers.Single(c => c.Token == token);
                    db.SaveChanges();
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
    }
}