﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using Nancy;
using Nancy.Routing;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.InterserviceCommunicators;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;

#pragma warning disable 1998

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Operator's information and service requests go here
    /// </summary>
    public class OperatorController : NancyModule
    {
        public OperatorController()
        {
            Get["/operator/sign/{in}"] = async (parameters, tok) =>
            {
                var email = Request.Query["email"];
                var pswd = Request.Query["password"];
                return SignIn(email, pswd);
            };
            
            Put["/operator/log/{out}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                return LogOut(token);
            };
            
            Get["/operator/delivery/{get}"] = async (parameters, tok) =>
            {
                var id = Request.Query["p_id"];
                var token = Request.Query["token"];
                return GetParcel(id, token);
            };

            Get["/operator/get/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetOperatorCurrentParcels(token);
            };
            
            Get["/operator/get/archived/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetOperatorArchivedParcels(token);
            };

            Get["/operator/get/new/{deliveries}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetNewParcels(token);
            };
            
            Put["/operator/confirm/{delivery}"] = async (parameters, tok) =>
            {
                var pId = Request.Form["parcel_id"];
                var drId = Request.Form["driver_id"];
                var routeId = Request.Form["route_id"];
                var token = Request.Form["token"];
                return ConfirmParcel(pId, drId, routeId, token);
            };
            
            Put["/operator/reject/{delivery}"] = async (parameters, tok) =>
            {
                var id = Request.Form["id"];
                var token = Request.Form["token"];
                var opMsg = Request.Form["op_reject_msg"];
                var custMsg = Request.Form["cust_reject_msg"];
                return RejectParcel(id, token, opMsg, custMsg);
            };

            Put["/operator/edit/{delivery}"] = async (parameters, tok) =>
            {
                var id = Request.Form["id"];
                var token = Request.Form["token"];
                var deliveryName = Request.Form["del_name"];
                var urgency = Request.Form["urgency"];
                var type = Request.Form["type"];
                var srcAddress = Request.Form["src_address"];
                var srcAddress2 = Request.Form["src_address_2"];
                var srcLat = Request.Form["src_latitude"];
                var srcLon = Request.Form["src_longitude"];
                var destAddress = Request.Form["dest_address"];
                var destAddress2 = Request.Form["dest_address_2"];
                var destLat = Request.Form["dest_latitude"];
                var destLon = Request.Form["dest_longitude"];
                return EditParcel(id, token, deliveryName, urgency, type,
                    srcAddress, srcAddress2, srcLat, srcLon,
                    destAddress, destAddress2, destLat, destLon);
            };

            Get["/operator/parcel/routes/{get}"] = async (parameters, tok) =>
            {
                var id = Request.Query["parcel_id"];
                var token = Request.Query["token"];
                return GetRoutes(id, token);
            };

            Get["/operator/drivers/parcels/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetDriversAndParcels(token);
            };
        }

        private static string SignIn(
            string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Email == email);
                    if (op == null)
                    {
                        response.status = ServerResponse.NoEmail;
                        return ServerUtilities.ToJson(response);
                    }
                    if (op.Password != ServerUtilities.GetHash(password))
                    {
                        response.status = ServerResponse.WrongPassword;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var tok = ServerUtilities.GetNewToken();
                    op.Token = tok;
                    
                    // Assign all unassigned parcels to this operator
                    var unassignedParcels = new List<Parcel>();
                    foreach (var parcel in db.Parcels.Where(p => p.Operator == null))
                    {
                        parcel.Operator = op;
                        unassignedParcels.Add(parcel);
                    }
                    db.SaveChanges();
                    
                    foreach (var parcel in unassignedParcels)
                        SendNewParcelNotification(op.Email, parcel.Id);

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = op.Id,
                        token = tok
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private string LogOut(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    op.Token = null;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetParcel(int id, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    var parcel = db.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongOperator;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new
                    {
                        id = parcel.Id,
                        name = parcel.Name,
                        source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                        src_lat = parcel.SourceLatitude,
                        src_lon = parcel.SourceLongitude,
                        destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                        dest_lat = parcel.DestinationLatitude,
                        dest_lon = parcel.DestinationLongitude,
                        urgency = parcel.Urgency,
                        type = parcel.Type,
                        status = parcel.Status.ToString(),
                        driver = parcel.Driver?.Id,
                        reject_msg_cust = parcel.RejectMessageForCustomer,
                        reject_msg_emp = parcel.RejectMessageForEmployees
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetOperatorCurrentParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcels = new List<object>();
                    foreach (var parcel in op.Parcels)
                    {
                        if (parcel.Status == Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new OperatorGetParcelsBody()
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            src_lat = parcel.SourceLatitude,
                            src_lon = parcel.SourceLongitude,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            dest_lat = parcel.DestinationLatitude,
                            dest_lon = parcel.DestinationLongitude,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString(),
                            driver = parcel.Driver?.Id,
                            reject_msg_cust = parcel.RejectMessageForCustomer,
                            reject_msg_emp = parcel.RejectMessageForEmployees
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string GetOperatorArchivedParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcels = new List<object>();
                    foreach (var parcel in op.Parcels)
                    {
                        if (parcel.Status != Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new OperatorGetParcelsBody()
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            src_lat = parcel.SourceLatitude,
                            src_lon = parcel.SourceLongitude,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            dest_lat = parcel.DestinationLatitude,
                            dest_lon = parcel.DestinationLongitude,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString(),
                            driver = parcel.Driver?.Id,
                            reject_msg_cust = parcel.RejectMessageForCustomer,
                            reject_msg_emp = parcel.RejectMessageForEmployees
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetNewParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcelsFormatted = new List<object>();
                    var newParcels = db.Parcels
                        .Where(p => p.Status == Parcel.ParcelStatus.Created && p.Operator.Id == op.Id)
                        .ToList();
                    foreach (var parcel in newParcels)
                    {
                        parcelsFormatted.Add(new OperatorGetNewParcelsBody()
                        {
                            id = parcel.Id,
                            operator_id = parcel.Operator?.Id ?? -1,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            src_lat = parcel.SourceLatitude,
                            src_lon = parcel.SourceLongitude,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            dest_lat = parcel.DestinationLatitude,
                            dest_lon = parcel.DestinationLongitude,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status.ToString()
                        });
                    }
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcelsFormatted;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string ConfirmParcel(
            int parcelId, int driverId, string routeId, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = op.Parcels.FirstOrDefault(p => p.Id == parcelId);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongOperator;
                        return ServerUtilities.ToJson(response);
                    }

                    var driver = db.Drivers.Find(driverId);
                    if (driver == null)
                    {
                        response.status = ServerResponse.NoDriver;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.ConfirmedByOperator, parcelId))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.Driver = driver;
                    parcel.ParcelAssessor.TimeConfirmedByOperator = DateTime.Now;
                    db.SaveChanges();

                    // Send notification to driver
                    if (parcel.Driver != null)
                        DriverController.SendNewParcelNotification(parcel.Driver.Email, parcelId);
                    
                    // Confirm the route on external server
                    var routeIsConfirmed = MapServiceCommunicator.ConfirmRoute(parcelId, routeId);
                    
                    if (!routeIsConfirmed)
                    {
                        response.status = ServerResponse.ExternalError;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string RejectParcel(
            int id, string token, string opRejectMessage, string customerRejectMessage)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcel = op.Parcels.FirstOrDefault(p => p.Id == id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.WrongOperator;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.Rejected, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }

                    parcel.RejectMessageForEmployees = opRejectMessage;
                    parcel.RejectMessageForCustomer = customerRejectMessage;
                    parcel.ParcelAssessor.TimeRejected = DateTime.Now;
                    parcel.IsRejected = true;
                    db.SaveChanges();
                    
                    ServerUtilities.ParcelStatusChanged(parcel.Id);

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string EditParcel(
            int id, string token, string name, string urgency, string type,             
            string srcAddress, string srcAddress2, string srcLat, string srcLon,
            string destAddress, string destAddress2, string destLat, string destLon)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    if (op.Parcels.All(p => p.Id != id))
                    {
                        // If the parcel is assigned to another operator
                        response.status = ServerResponse.WrongOperator;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcel = db.Parcels.Find(id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.Name = name;
                    parcel.Urgency = urgency;
                    parcel.Type = type;
                    parcel.SourceAddress = srcAddress;
                    parcel.SourceAddressExtra = srcAddress2;
                    parcel.SourceLatitude = srcLat;
                    parcel.SourceLongitude = srcLon;
                    parcel.DestinationAddress = destAddress;
                    parcel.DestinationAddressExtra = destAddress2;
                    parcel.DestinationLatitude = destLat;
                    parcel.DestinationLongitude = destLon;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetRoutes(int id, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    if (op.Parcels.All(p => p.Id != id))
                    {
                        // If the parcel is assigned to another operator
                        response.status = ServerResponse.WrongOperator;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }

                    var routes = MapServiceCommunicator.GetRoutes(
                        parcel.SourceAddress, parcel.SourceLatitude, parcel.SourceLongitude,
                        parcel.DestinationAddress, parcel.DestinationLatitude, parcel.DestinationLongitude);

                    if (routes == null)
                    {
                        response.status = ServerResponse.ExternalError;
                        return ServerUtilities.ToJson(response);
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = routes;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetDriversAndParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var op = db.Operators.FirstOrDefault(e => e.Token == token);
                    if (op == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var driversAndParcels = new Dictionary<string, List<int>>();
                    foreach (var parcel in op.Parcels)
                    {
                        // Do not consider parcels, which are not picked up yet or
                        // already delivered ones
                        if (parcel.Status != Parcel.ParcelStatus.PickedUp)
                            continue;
                        
                        var currentDriverId = parcel.DriverId.ToString();
                        if (driversAndParcels.ContainsKey(currentDriverId))
                            driversAndParcels[currentDriverId].Add(parcel.Id);
                        else
                        {
                            var parcelsList = new List<int> {parcel.Id};
                            driversAndParcels.Add(currentDriverId, parcelsList);
                        }
                    }

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = driversAndParcels;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        /// <summary>
        /// Just a little incapsulation for this class to send notifications
        /// </summary>
        private static bool SendNewParcelNotification(
            string email, int id)
        {
            return NotificationServiceCommunicator.SendNewParcelNotificaton(
                NotificationServiceCommunicator.Role.Operator, email, id);
        }
    }
}