﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Nancy;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Database.Models;
using ParcelsManagementServer.Server.InterserviceCommunicators;
using ParcelsManagementServer.Server.Models;
using ParcelsManagementServer.Server.Utilities;
#pragma warning disable 1998

namespace ParcelsManagementServer.Server.Controllers
{
    /// <summary>
    /// Manager and overseer requests
    /// </summary>
    public class ManagerController : NancyModule
    {
        public ManagerController()
        {
            Get["/manager/sign/{in}"] = async (parameters, tok) =>
            {
                var email = Request.Query["email"];
                var pswd = Request.Query["password"];
                return SignIn(email, pswd);
            };
            
            Put["/manager/log/{out}"] = async (parameters, tok) =>
            {
                var token = Request.Form["token"];
                return LogOut(token);
            };
            
            Put["/manager/operator/new/{register}"] = async (parameters, tok) =>
            {
                var email = Request.Form["email"];
                var name = Request.Form["name"];
                var pswd = Request.Form["password"];
                var token = Request.Form["token"];
                return RegisterOperator(email, name, pswd, token);
            };

            Put["/manager/driver/new/{register}"] = async (parameters, tok) =>
            {
                var email = Request.Form["email"];
                var name = Request.Form["name"];
                var pswd = Request.Form["password"];
                var phone = Request.Form["phone"];
                var token = Request.Form["token"];
                return RegisterDriver(email, name, pswd, phone, token);
            };

            Get["/manager/operator/all/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetAllOperators(token);
            };

            Get["/manager/driver/all/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetAllDrivers(token);
            };
            
            Get["/manager/parcel/all/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetAllCurrentParcels(token);
            };      
            
            Get["/manager/parcel/all/archived/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetAllArchivedParcels(token);
            }; 

            Put["/manager/operator/assign/{parcel}"] = async (parameters, tok) =>
            {
                var pId = Request.Form["parcel_id"];
                var opId = Request.Form["op_id"];
                var token = Request.Form["token"];
                return AssignParcelToOperator(pId, opId, token);
            };

            Put["/manager/driver/assign/{parcel}"] = async (parameters, tok) =>
            {
                var pId = Request.Form["parcel_id"];
                var drId = Request.Form["dr_id"];
                var token = Request.Form["token"];
                return AssignParcelToDriver(pId, drId, token);
            };

            Get["/manager/assessment/get/{criteria}"] = async (parameters, tok) =>
            {
                var pId = Request.Query["parcel_id"];
                var token = Request.Query["token"];
                return GetAssessmentCriteria(pId, token);
            };
            
            Put["/manager/assessment/assess/{parcel}"] = async (parameters, tok) =>
            {
                var pId = Request.Form["parcel_id"];
                var token = Request.Form["token"];
                var mark = Request.Form["mark"];
                var comment = Request.Form["comment"];
                return AssessParcel(pId, mark, comment, token);
            };
            
            Get["/manager/statistics/{get}"] = async (parameters, tok) =>
            {
                var token = Request.Query["token"];
                return GetStatistics(token);
            };
        }
        
        private static string SignIn(
            string email, string password)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var manager = db.Managers.FirstOrDefault(m => m.Email == email);
                    if (manager == null)
                    {
                        response.status = ServerResponse.NoEmail;
                        return ServerUtilities.ToJson(response);
                    }
                    if (manager.Password != ServerUtilities.GetHash(password))
                    {
                        response.status = ServerResponse.WrongPassword;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var tok = ServerUtilities.GetNewToken();
                    manager.Token = tok;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = manager.Id,
                        token = tok
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private string LogOut(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    var manager = db.Managers.FirstOrDefault(m => m.Token == token);
                    if (manager == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    manager.Token = null;
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string RegisterOperator(
            string email, string name, string password, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Operators.FirstOrDefault(e => e.Email == email) != null)
                    {
                        response.status = ServerResponse.UserAlreadyRegistered;
                        return ServerUtilities.ToJson(response);
                    }
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var op = db.Operators.Add(new Operator()
                    {
                        Name = name,
                        Email = email,
                        Password = ServerUtilities.GetHash(password)
                    });
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = op.Id
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
              
        private static string RegisterDriver(
            string email, string name, string password, string phone, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Drivers.FirstOrDefault(d => d.Email == email) != null)
                    {
                        response.status = ServerResponse.UserAlreadyRegistered;
                        return ServerUtilities.ToJson(response);
                    }
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var driver = db.Drivers.Add(new Driver()
                    {
                        Name = name,
                        Email = email,
                        Password = ServerUtilities.GetHash(password),
                        Phone = phone
                    });
                    db.SaveChanges();

                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = new RegisterLoginBody()
                    {
                        id = driver.Id
                    };
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetAllOperators(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var operators = new List<object>();
                    foreach (var op in db.Operators)
                    {
                        operators.Add(new
                        {
                            id = op.Id,
                            name = op.Name,
                            email = op.Email,
                            is_online = op.Token != null,
                            parcels = op.Parcels.Select(p => p.Id).ToArray()
                        });
                    }
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = operators;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetAllDrivers(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var drivers = new List<object>();
                    foreach (var driver in db.Drivers)
                    {
                        drivers.Add(new
                        {
                            id = driver.Id,
                            name = driver.Name,
                            email = driver.Email,
                            phone = driver.Phone,
                            is_online = driver.Token != null,
                            parcels = driver.Parcels.Select(p => p.Id).ToArray()
                        });
                    }
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = drivers;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetAllCurrentParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcels = new List<object>();
                    foreach (var parcel in db.Parcels)
                    {
                        if (parcel.Status == Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status,
                            ass_operator = parcel.OperatorId,
                            ass_driver = parcel.DriverId
                        });
                    }
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string GetAllArchivedParcels(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcels = new List<object>();
                    foreach (var parcel in db.Parcels)
                    {
                        if (parcel.Status != Parcel.ParcelStatus.Archived)
                            continue;
                        parcels.Add(new
                        {
                            id = parcel.Id,
                            name = parcel.Name,
                            source = parcel.SourceAddress + " " + parcel.SourceAddressExtra,
                            destination = parcel.DestinationAddress + " " + parcel.DestinationAddressExtra,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            status = parcel.Status,
                            ass_operator = parcel.OperatorId,
                            ass_driver = parcel.DriverId
                        });
                    }
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = parcels;
                    return ServerUtilities.ToJson(response);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string AssignParcelToOperator(
            int parcelId, int operatorId, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var op = db.Operators.Find(operatorId);
                    if (op == null)
                    {
                        response.status = ServerResponse.NoOperator;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(parcelId);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.OperatorId = op.Id;
                    db.SaveChanges();

                    NotificationServiceCommunicator.SendNewParcelNotificaton(
                        NotificationServiceCommunicator.Role.Operator, op.Email, parcel.Id);
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }
        
        private static string AssignParcelToDriver(
            int parcelId, int driverId, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var driver = db.Drivers.Find(driverId);
                    if (driver == null)
                    {
                        response.status = ServerResponse.NoDriver;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(parcelId);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.DriverId = driver.Id;
                    db.SaveChanges();

                    NotificationServiceCommunicator.SendNewParcelNotificaton(
                        NotificationServiceCommunicator.Role.Driver, driver.Email, parcel.Id);
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetAssessmentCriteria(int id, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }
                    if (parcel.Status != Parcel.ParcelStatus.AssessedByCustomer &&
                        parcel.Status != Parcel.ParcelStatus.Rejected)
                    {
                        response.status = ServerResponse.ParcelNotDeliveredOrRejected;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcelAssessor = parcel.ParcelAssessor;
                    var criteria = new
                    {
                        time_created = parcelAssessor.TimeCreated,
                        time_confirmed_by_operator = parcelAssessor.TimeConfirmedByOperator,
                        time_confirmed_by_driver = parcelAssessor.TimeConfirmedByDriver,
                        time_picked_up = parcelAssessor.TimePickedUp,
                        time_delivered = parcelAssessor.TimeDelivered,
                        time_cust_assessed = parcelAssessor.TimeAssessedByCustomer,
                        time_assessed = parcelAssessor.TimeAssessedByManager,
                        time_rejected = parcelAssessor.TimeRejected,
                        cust_mark = parcelAssessor.CustomerMark,
                        cust_comment = parcelAssessor.CustomerComment,
                        reject_msg_cust = parcel.RejectMessageForCustomer,
                        reject_msg_emp = parcel.RejectMessageForEmployees
                    };
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = criteria;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string AssessParcel(int id, float mark, string comment, string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    var parcel = db.Parcels.Find(id);
                    if (parcel == null)
                    {
                        response.status = ServerResponse.NoParcel;
                        return ServerUtilities.ToJson(response);
                    }

                    if (!ParcelStatusChangeEvent.ChangeParcelStatus(Parcel.ParcelStatus.Archived, id))
                    {
                        response.status = ServerResponse.WrongStatusChangeAttempt;
                        return ServerUtilities.ToJson(response);
                    }
                    
                    parcel.ParcelAssessor.ManagerMark = mark;
                    parcel.ParcelAssessor.ManagerComment = comment;
                    parcel.ParcelAssessor.TimeAssessedByManager = DateTime.Now;
                    parcel.ParcelAssessor.Manager = db.Managers.Single(m => m.Token == token);
                    db.SaveChanges();

                    var archiveEntryId = ArchiveParcel(parcel.CustomerId, parcel.Id);
                    if (archiveEntryId != -1)
                        parcel.ArchiveEntryId = archiveEntryId;
                    
                    db.SaveChanges();
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static string GetStatistics(string token)
        {
            var response = new ServerResponse();
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    if (db.Managers.FirstOrDefault(m => m.Token == token) == null &&
                        db.Overseers.FirstOrDefault(o => o.Token == token) == null)
                    {
                        response.status = ServerResponse.WrongToken;
                        return ServerUtilities.ToJson(response);
                    }

                    var parcels = db.Parcels;
                    var operators = db.Operators;
                    var drivers = db.Drivers;
                    var managers = db.Managers;
                    var deliveredOrArchivedParcels = parcels
                        .Where(p => (
                            p.Status == Parcel.ParcelStatus.Archived ||
                            p.Status == Parcel.ParcelStatus.Delivered) && !p.IsRejected);
                    var stats = new
                    {
                        users = new
                        {
                            op_total = operators.Count(),
                            op_online = operators.Count(o => o.Token != null),
                            dr_total = drivers.Count(),
                            dr_online = drivers.Count(d => d.Token != null),
                            managers_total = managers.Count(),
                            managers_online = managers.Count(m => m.Token != null)
                        },
                        parcels = new
                        {
                            parcels_total = parcels.Count(),
                            parcels_created = parcels.Count(p => p.Status == Parcel.ParcelStatus.Created),
                            parcels_op_confirmed = parcels.Count(
                                p => p.Status == Parcel.ParcelStatus.ConfirmedByOperator),
                            parcels_dr_confirmed = parcels.Count(
                                p => p.Status == Parcel.ParcelStatus.ConfirmedByDriver),
                            parcels_picked_up = parcels.Count(p => p.Status == Parcel.ParcelStatus.PickedUp),
                            parcels_delivered = parcels.Count(p => p.Status == Parcel.ParcelStatus.Delivered),
                            parcels_cust_assessed = parcels.Count(p => p.Status == Parcel.ParcelStatus.AssessedByCustomer),
                            parcels_archived = parcels.Count(p => p.Status == Parcel.ParcelStatus.Archived),
                            parcels_rejected = parcels.Count(p => p.Status == Parcel.ParcelStatus.Rejected),
                            op_avg_parcels = operators.Sum(o => o.Parcels.Count) / db.Operators.Count(),
                            dr_avg_parcels = drivers.Sum(d => d.Parcels.Count) / db.Drivers.Count()
                        },
                        intervals = new
                        {
                            avg_delivery_time = deliveredOrArchivedParcels.Any() 
                                ?
                                        deliveredOrArchivedParcels.Sum(p => DbFunctions.DiffSeconds(
                                            p.ParcelAssessor.TimeCreated, p.ParcelAssessor.TimeDelivered).Value)
                                    /
                                        deliveredOrArchivedParcels.Count()
                                :
                                    0
                        }
                    };
                    
                    response.status = ServerResponse.SuccessfullResponse;
                    response.result = stats;
                    return ServerUtilities.ToJson(response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.status = ServerResponse.InternalError;
                return ServerUtilities.ToJson(response);
            }
        }

        private static int ArchiveParcel(int customerId, int parcelId)
        {
            return ArchiveServiceCommunicator.ArchiveParcel(customerId, parcelId);
        }
    }
}