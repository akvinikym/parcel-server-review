﻿using System.Net;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server.InterserviceCommunicators
{
    public static class WeatherTrafficServiceCommunicator
    {
        private const string ServiceAddress = "http://18.220.203.246:4080";
        private const string SubscriptionAddress = "/subscribe";

        public static bool SubscribeDriver(int parcelId)
        {
            using (var db = new ParcelsManagementDBContext())
            {
                var parcel = db.Parcels.Find(parcelId);
                var request = new
                {
                    operator_email = parcel.Operator.Email,
                    driver_email = parcel.Driver.Email,
                    driver_id = parcel.Driver.Id
                };

                var response = ServerUtilities.PostHttpJsonRequest(ServiceAddress + SubscriptionAddress, 
                        request).Result;

                return response.Item1 == HttpStatusCode.OK;
            }
        }
    }
}