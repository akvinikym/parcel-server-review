﻿using System.Net;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server.InterserviceCommunicators
{
    public class MapServiceCommunicator
    {
        private const string MapServiceIP = "http://18.220.203.246:7080";
        private const string GetRoutesAddress = "/routes/calculate";
        private const string ConfirmRouteAddress = "/routes/confirm";
        
        /// <summary>
        /// Get routes by source and destination addresses; returns null, if error happened
        /// </summary>
        public static string GetRoutes(
            string sourceAddress, string sourceLatitude, string sourceLongitude,
            string destAdrress, string destLatitude, string destLongitude)
        {
            var response = ServerUtilities.PostHttpJsonRequest(MapServiceIP + GetRoutesAddress, new
            {
                src_address = sourceAddress,
                src_lat = sourceLatitude,
                src_lon = sourceLongitude,
                dest_address = destAdrress,
                dest_lat = destLatitude,
                dest_lon = destLongitude
            }).Result;

            return response.Item1 == HttpStatusCode.OK ? response.Item2 : null;
        }

        /// <summary>
        /// Confirm the route on route service
        /// </summary>
        public static bool ConfirmRoute(int parcelId, string routeId)
        {
            var response = ServerUtilities.PostHttpJsonRequest(MapServiceIP + ConfirmRouteAddress, new
            {
                parcel_id = parcelId,
                route_id = routeId
            }).Result;

            return response.Item1 == HttpStatusCode.OK;
        }
    }
}