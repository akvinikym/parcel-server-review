﻿using System;
using System.Net;
using System.Web.Helpers;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server.InterserviceCommunicators
{
    public static class ArchiveServiceCommunicator
    {
        private const string ArchiveServiceAddress = "http://18.220.203.246:6080";
        private const string StoreParcelAddress = "/parcel/store";

        /// <summary>
        /// Store the parcel in the external service's archive. Returns -1, if something
        /// went wrong, and integer > 0, which stays for record_id in this service
        /// </summary>
        public static int ArchiveParcel(int customerId, int parcelId)
        {
            using (var db = new ParcelsManagementDBContext())
            {
                var parcel = db.Parcels.Find(parcelId);
                var parcelAssessor = parcel.ParcelAssessor;

                var parcelObject = new
                {
                    id = parcelId,
                    name = parcel.Name,
                    source = parcel.SourceAddress,
                    destination = parcel.DestinationAddress,
                    urgency = parcel.Urgency,
                    type = parcel.Type,
                    weight = parcel.Weight
                };
                var assessmentObject = new
                {
                    mark = parcelAssessor.ManagerMark,
                    comment = parcelAssessor.ManagerComment,
                    manager_id = parcelAssessor.Manager.Id
                };
                var criteria = new
                {
                    time_created = parcelAssessor.TimeCreated.GetUnixEpoch(),
                    time_confirmed_by_operator = parcelAssessor.TimeConfirmedByOperator.GetUnixEpoch(),
                    time_confirmed_by_driver = parcelAssessor.TimeConfirmedByDriver.GetUnixEpoch(),
                    time_picked_up = parcelAssessor.TimePickedUp.GetUnixEpoch(),
                    time_delivered = parcelAssessor.TimeDelivered.GetUnixEpoch(),
                    time_assessed = parcelAssessor.TimeAssessedByManager.GetUnixEpoch(),
                    time_rejected = parcelAssessor.TimeRejected.GetUnixEpoch(),
                    reject_comment = parcel.RejectMessageForEmployees ?? ""
                };
                var request = new
                {
                    customer_id = customerId,
                    parcel = parcelObject,
                    assessment = assessmentObject,
                    assesment_criteria = criteria
                };

                var response = ServerUtilities.PostHttpJsonRequest(
                    ArchiveServiceAddress + StoreParcelAddress, request).Result;

                return response.Item1 == HttpStatusCode.Created 
                    ? Json.Decode(response.Item2)["record_id"] 
                    : -1;
            }
        }
    }
}