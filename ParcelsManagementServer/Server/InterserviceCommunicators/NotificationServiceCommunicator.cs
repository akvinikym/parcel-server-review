﻿using System;
using System.Linq;
using System.Net;
using System.Web.Helpers;
using Microsoft.Win32.SafeHandles;
using ParcelsManagementServer.Database;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server.InterserviceCommunicators
{
    /// <summary>
    /// Methods for sending notifications about various events to
    /// several user roles
    /// </summary>
    public static class NotificationServiceCommunicator
    {
        private const string NotificationServerIP = "http://18.220.203.246:8080";
        private const string NewParcelToOperatorAddress = "/operator/newdelivery";
        private const string NewParcelToDriverAddress = "/driver/newdelivery";
        private const string StatusChangeAddress = "/multicast/status_change";
        private const string EmergencyAddress = "/operator/emergency";
        
        public enum Role
        {
            Operator,
            Driver
        }

        /// <summary>
        /// Send a new parcel notification using a post request with a provided Json body
        /// </summary>
        public static bool SendNewParcelNotificaton(
            Role role, string email, int parcelId)
        {
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    // Pick the right request depending on the passed role
                    var requestAddress = NotificationServerIP;
                    switch (role)
                    {
                        case Role.Operator:
                            requestAddress += NewParcelToOperatorAddress;
                            break;
                        case Role.Driver:
                            requestAddress += NewParcelToDriverAddress;
                            break;
                        default: return false;
                    }

                    // Build a parcel object to be sent
                    var parcel = db.Parcels.Single(p => p.Id == parcelId);
                    var request = new
                    {
                        email = email,
                        parcel = new
                        {
                            id = parcelId,
                            name = parcel.Name,
                            source = parcel.SourceAddress,
                            destination = parcel.DestinationAddress,
                            urgency = parcel.Urgency,
                            type = parcel.Type,
                            weight = parcel.Weight,
                            status = parcel.Status
                        }
                    };

                    // Post a request and process response
                    var response = ServerUtilities.PostHttpJsonRequest(requestAddress, request).Result;
                    var status = response.Item1;

                    if (status == HttpStatusCode.OK)
                        return true;
                    
                    Console.WriteLine("\nNotification Error: " + 
                                      Json.Decode(response.Item2)["error"] + "\n\n");
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nNotification was not send because of exception!" + e.Message + "\n\n");
                return false;
            }
        }

        /// <summary>
        /// Event handler for status change
        /// </summary>
        public static void HandleStatusChange(
            object sender, ParcelStatusChangeEvent.ParcelStatusChangeEventArgs e)
        {
            SendChangeStatusNotification(e.ParcelId);
        }

        /// <summary>
        /// Send a notification, when status of the parcel changes
        /// </summary>
        private static bool SendChangeStatusNotification(int id)
        {
            try
            {
                using (var db = new ParcelsManagementDBContext())
                {
                    // Create a request
                    var requestAddress = NotificationServerIP + StatusChangeAddress;
                    var parcel = db.Parcels.Single(p => p.Id == id);
                    var request = new
                    {
                        emails = new[]
                        {
                            parcel.Operator.Email,
                            parcel.Driver?.Email,
                            parcel.Customer.Email
                        },
                        id = id,
                        status = parcel.Status.ToString()
                    };
                    
                    // Post a request
                    var response = ServerUtilities.PostHttpJsonRequest(requestAddress, request).Result;
                    var status = response.Item1;

                    if (status == HttpStatusCode.OK)
                        return true;

                    Console.WriteLine("\nNotification Error: " + 
                                      Json.Decode(response.Item2)["error"] + "\n\n");
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nNotification was not send because of exception!" + e.Message + "\n\n");
                return false;
            }
        }

        /// <summary>
        /// Send a notification, if driver activates an emergency button
        /// </summary>
        public static bool SendEmergencyNotification(int driverId, string driverPhone, string opEmail)
        {
            try
            {
                var requestAddress = NotificationServerIP + EmergencyAddress;
                var request = new
                {
                    driver_id = driverId,
                    driver_phone_number = driverPhone,
                    operator_email = opEmail
                };

                var response = ServerUtilities.PostHttpJsonRequest(requestAddress, request).Result;
                var status = response.Item1;

                if (status == HttpStatusCode.OK)
                    return true;

                Console.WriteLine("\nNotification Error: " + 
                                  Json.Decode(response.Item2)["error"] + "\n\n");
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nNotification was not send because of exception!" + e.Message + "\n\n");
                return false;
            }
        }
    }
}