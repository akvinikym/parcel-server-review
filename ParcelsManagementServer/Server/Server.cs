﻿using System;
using Nancy.Hosting.Self;
using System.Web.Script.Serialization;
using ParcelsManagementServer.Server.Utilities;

namespace ParcelsManagementServer.Server
{
    public class Server
    {
        public static void Init()
        {
            try
            { 
                // Make the URL creation possible
                var hostConfiguration = new HostConfiguration
                {
                    UrlReservations = new UrlReservations()
                    {
                        CreateAutomatically = true
                    }
                };
                
                // Configure server events
                ServerUtilities.ConfigureServerEvents();

                // Main loop
                using (var host = new NancyHost(
                    hostConfiguration, new Uri("http://localhost:4377")))
                {
                    host.Start();
                    Console.WriteLine("Server is up! Enter 'exit' to quit");
                    string command;
                    do
                    {
                        command = Console.ReadLine();
                    } while (command != "exit");
                    host.Stop();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e + "\n\n");
            }
        }
    }
}