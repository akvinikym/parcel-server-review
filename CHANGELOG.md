# Changelog
This document is created for tracing changes in the system's API.

## [0.9.9z] - 11.11.2017
### Added
 - Customer feature to assess the parcel before the Manager does so
 - Status for the parcel, which was assessed by Customer, but not by Manager

### Changed
 - GetAssessmentCriteria - now it has more fields, which concern Customer's assessment
 - Requests to get the parcels for Customer, Operator and Manager are now splitted to two each: get current parcels and get archived ones

## [0.1.5] - 02.11.2017
### Added
- Operator feature to get IDs of Drivers with parcels this Operator is assigned to
- Operator feature to get details of a particular parcels
- Driver feature for an emergency button emulation
- Manager feature to get statistics about the system
- Server responses: one for incorrect status change and the other for no-operators-online case

### Changed
- Reject message is now splitted to two: inner one and for Customer
- Driver has a phone number
- Default token for Operator is being set after each DB drop

## [0.1.2] - 27.10.2017
### Added
- Customer confirmation of parcel delivery
- Driver Pick Up feature
- Reject message for Operator
- Parcel Assessment by Manager

### Changed
- Get Parcels for Operator and Customer

## [0.0.9b] - 22.10.2017
### Added
- Routes features
- Pre-created users for each role

### Changed
- Create parcel
- Edit parcel
- Operator parcel confirm

## [0.0.9] - 15.10.2017
### Added
- Notifications support
- Reject and Edit features in operator's API
- Manager and Overseer roles

### Changed
- Only Manager can register new users now

## [0.0.5] - 07.10.2017
### Added
- Register, Log In and Log Out functions for all the roles

### Changed
- Create parcels feature 

## [0.0.1] - 02.10.2017
### Added
- README.MD file with all current API's functions
- Initial commit